package com.song.solartube.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.song.solartube.ApiCall;
import com.song.solartube.DefaultRestClient;
import com.song.solartube.R;
import com.song.solartube.singleton.MoveManager;
import com.song.solartube.fragment.AdultFragment;
import com.song.solartube.adapter.holder.AdultHolder;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.ContentThumbnail;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-11-08.
 */

public class AdultListAdapter extends RecyclerView.Adapter<AdultHolder> implements
        Callback<List<ContentThumbnail>>, OnClickThumbnailListener {

    private List<ContentThumbnail> thumbnailList = new ArrayList<>();
    private Call<List<ContentThumbnail>> call;

    private DefaultRestClient<ApiCall> defaultRestClient;
    private ApiCall apiCall;
    private AdultFragment fragment;

    public AdultListAdapter(AdultFragment adultFragment, Call<List<ContentThumbnail>> call) {
        this.fragment = adultFragment;
        this.defaultRestClient = new DefaultRestClient<>();
        this.apiCall = defaultRestClient.getClient(ApiCall.class);
        this.call = call;
        this.call.enqueue(this);
    }

    @Override
    public AdultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        LinearLayout linearLayout = (LinearLayout)inflater
                .inflate(R.layout.holder_adult_thumbnail, parent, false);

        AdultHolder adultHolder = new AdultHolder(linearLayout);
        return adultHolder;
    }

    @Override
    public void onBindViewHolder(AdultHolder holder, int position) {
        holder.setThumbnail(this, thumbnailList.get(position));
    }

    @Override
    public int getItemCount() {
        return thumbnailList.size();
    }

    @Override
    public void onResponse(Call<List<ContentThumbnail>> call, Response<List<ContentThumbnail>> response) {
        thumbnailList = response.body();
        notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<List<ContentThumbnail>> call, Throwable t) {

    }

    @Override
    public void onClickThumbnail(final ContentThumbnail thumbnail) {
        Call<List<Content>> call = apiCall.getAdultContentDetailList(thumbnail.title, thumbnail.country);

        call.enqueue(new Callback<List<Content>>() {
            @Override
            public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
                List<Content> contents = response.body();
                MoveManager.getInstance().moveContentDetailPage(fragment.getContext(), thumbnail.title, contents);
            }

            @Override
            public void onFailure(Call<List<Content>> call, Throwable t) {
                call.clone().enqueue(this);
            }
        });
    }
}
