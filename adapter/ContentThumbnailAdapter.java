package com.song.solartube.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.song.solartube.R;
import com.song.solartube.base.BaseApplication;
import com.song.solartube.adapter.contract.ContentThumbnailAdapterContract;
import com.song.solartube.fragment.MainFragment;
import com.song.solartube.adapter.holder.ContentThumbnailHolder;
import com.song.solartube.adapter.holder.LoadingViewHolder;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.ContentThumbnail;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.MoveManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Song on 2017-09-12.
 */

public class ContentThumbnailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements OnClickThumbnailListener, ContentThumbnailAdapterContract.View, ContentThumbnailAdapterContract.Model{

    private List<ContentThumbnail> thumbnailList = new ArrayList<>();
    private MainFragment fragment;

    private OnClickThumbnailListener onClickThumbnailListener;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private boolean isLast;

    public ContentThumbnailAdapter(MainFragment fragment) {
        this.fragment = fragment;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public int getThumbnailsCount() {
        return thumbnailList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_ITEM:
                LinearLayout linearLayout = (LinearLayout)inflater
                        .inflate(R.layout.holder_content_thumbnail, parent, false);

                return new ContentThumbnailHolder(linearLayout);
            case VIEW_TYPE_LOADING:
                LinearLayout loading = (LinearLayout)inflater
                        .inflate(R.layout.holder_loading, parent, false);
                return new LoadingViewHolder(loading);
        }

        return null;

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_ITEM:
                ContentThumbnailHolder contentHolder = (ContentThumbnailHolder)holder;
                configureThumbnailHolder(thumbnailList.get(position), contentHolder);
                break;
            case VIEW_TYPE_LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder)holder;
                configureLoadingHolder(loadingViewHolder);
                break;
        }
    }

    private void configureThumbnailHolder(ContentThumbnail content, ContentThumbnailHolder holder) {
        String url = content.thumbnail;

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .priority(Priority.IMMEDIATE);

        Glide.with(fragment).load(url).transition(withCrossFade()).apply(options).into(holder.getHolderImageView());
        holder.setData(this.onClickThumbnailListener, content);
    }

    private void configureLoadingHolder(LoadingViewHolder loadingViewHolder) {

    }

    @Override
    public int getItemCount() {
        if(thumbnailList != null){
            if(isLast) {
                return thumbnailList.size();
            } else {
                return thumbnailList.size() + 1;
            }
        }

        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == thumbnailList.size()) {
            return VIEW_TYPE_LOADING;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public void onClickThumbnail(final ContentThumbnail thumbnail) {
        Call<List<Content>> call = APIManager.getInstance().getApiCall().getContentDetailList(thumbnail.subtitle, thumbnail.country);
        BaseApplication.getInstance().progressOn(fragment.getActivity(), "로딩중..", call);

        call.enqueue(new Callback<List<Content>>() {
            @Override
            public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
                List<Content> contents = response.body();

                if(contents.size() != 1) {
                    MoveManager.getInstance().moveContentListPage(fragment.getContext(), thumbnail.title, contents);
                } else {
                    MoveManager.getInstance().moveContentDetailPage(fragment.getContext(), thumbnail.title, contents);
                }
            }

            @Override
            public void onFailure(Call<List<Content>> call, Throwable t) {
                call.clone().enqueue(this);
            }
        });
    }


    @Override
    public void setOnClickListener(OnClickThumbnailListener onClickThumbnailListener) {
        this.onClickThumbnailListener = onClickThumbnailListener;
    }

    @Override
    public void notifyAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<ContentThumbnail> imageItems) {
        if(imageItems.size() != 0) {
            thumbnailList.addAll(imageItems);
        } else {
            isLast = true;
        }
    }

    @Override
    public void clearItem() {

    }

    @Override
    public String getItem(int position) {
        return null;
    }
}
