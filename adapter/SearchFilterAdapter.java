package com.song.solartube.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.song.solartube.adapter.holder.SearchFilterHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Song on 2017-09-07.
 */

public class SearchFilterAdapter extends RecyclerView.Adapter<SearchFilterHolder> {

    private List<String> searchAllList = new ArrayList<>();
    private List<String> searchResultList = new ArrayList<>();

    public SearchFilterAdapter(List<String> list){

    }

    @Override
    public SearchFilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(SearchFilterHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return searchResultList.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        searchAllList.clear();

        if (charText.length() != 0) {
            for (String text : searchAllList) {
                if (text.toLowerCase().contains(charText)) {
                    searchResultList.add(text);
                } else {

                }
            }
        } else {
            searchResultList.addAll(searchAllList);
        }

        notifyDataSetChanged();
    }
}
