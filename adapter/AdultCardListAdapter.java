package com.song.solartube.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.song.solartube.ApiCall;
import com.song.solartube.DefaultRestClient;
import com.song.solartube.R;
import com.song.solartube.activity.ContentDetailActivity;
import com.song.solartube.fragment.AdultFragment;
import com.song.solartube.adapter.holder.AdultCardHolder;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.ContentThumbnail;
import com.song.solartube.model.ThumbnailModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Song on 2017-11-09.
 */

public class AdultCardListAdapter extends RecyclerView.Adapter<AdultCardHolder> implements
        Callback<List<ContentThumbnail>>, OnClickThumbnailListener {

    private List<ContentThumbnail> thumbnailList = new ArrayList<>();
    private Call<List<ContentThumbnail>> call;

    private DefaultRestClient<ApiCall> defaultRestClient;
    private ApiCall apiCall;
    private AdultFragment fragment;

    public AdultCardListAdapter(AdultFragment adultFragment, Call<List<ContentThumbnail>> call) {
        this.fragment = adultFragment;
        this.defaultRestClient = new DefaultRestClient<>();
        this.apiCall = defaultRestClient.getClient(ApiCall.class);
        this.call = call;
        this.call.enqueue(this);
    }

    @Override
    public AdultCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        LinearLayout cardView = (LinearLayout)inflater
                .inflate(R.layout.holder_adult_cradview, parent, false);

        AdultCardHolder adultCardHolder = new AdultCardHolder(cardView);
        return adultCardHolder;
    }

    @Override
    public void onBindViewHolder(AdultCardHolder holder, int position) {
        String url = thumbnailList.get(position).thumbnail;

        RequestOptions options = new RequestOptions()

                //.placeholder(R.drawable.ic_photo_white_24dp)
                .priority(Priority.HIGH);

        Glide.with(fragment).load(url).transition(withCrossFade()).apply(options).into(holder.imageView);
        holder.setData(this, thumbnailList.get(position));
    }

    @Override
    public int getItemCount() {
        return thumbnailList.size();
    }

    @Override
    public void onResponse(Call<List<ContentThumbnail>> call, Response<List<ContentThumbnail>> response) {
        thumbnailList = response.body();
        notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<List<ContentThumbnail>> call, Throwable t) {

    }

    @Override
    public void onClickThumbnail(ContentThumbnail thumbnail) {
        Call<List<Content>> call = apiCall.getAdultContentDetailList(thumbnail.title, thumbnail.country);
        call.enqueue(new Callback<List<Content>>() {
            @Override
            public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
                List<Content> contents = response.body();
                String[] ids = new String[contents.size()];
                int[] parts = new int[contents.size()];

                for (int i=0; i<contents.size(); i++) {
                    String id = contents.get(i).contentId;
                    int part = contents.get(i).part;
                    ids[i] = id;
                    parts[i] = part;
                    ThumbnailModel.getInstance().addContentDetail(id, contents.get(i));
                }

                Intent intent = new Intent(fragment.getContext(), ContentDetailActivity.class);
                intent.putExtra("parts", parts);
                intent.putExtra("ids", ids);
                fragment.startActivity(intent);
            }

            @Override
            public void onFailure(Call<List<Content>> call, Throwable t) {
                call.clone().enqueue(this);
            }
        });
    }
}
