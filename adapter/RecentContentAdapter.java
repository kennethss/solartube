package com.song.solartube.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.bumptech.glide.Glide;
import com.song.solartube.R;
import com.song.solartube.base.BaseApplication;
import com.song.solartube.callback.BasicResultCallback;
import com.song.solartube.adapter.contract.RecentAdatperContract;
import com.song.solartube.adapter.holder.RecentContentHolder;
import com.song.solartube.listener.OnClickRecentThumbnailListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.VideoLog;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.DialogManager;
import com.song.solartube.singleton.MoveManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Song on 2017-11-21.
 */

public class RecentContentAdapter extends RecyclerView.Adapter<RecentContentHolder>
        implements OnClickRecentThumbnailListener, SingleButtonCallback,
        RecentAdatperContract.View, RecentAdatperContract.Model {

    private List<VideoLog> logList = new ArrayList<>();
    private Activity activity;

    public RecentContentAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public RecentContentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        LinearLayout layout = (LinearLayout)inflater
                .inflate(R.layout.holder_recent_content, parent, false);

        return new RecentContentHolder(layout);
    }

    @Override
    public void onBindViewHolder(RecentContentHolder holder, int position) {
        VideoLog log = logList.get(position);

        Glide.with(activity).load(log.thumbnail).transition(withCrossFade()).into(holder.getThumbnailView());
        holder.setData(log, this);
    }

    @Override
    public int getItemCount() {
        if(logList != null) {
            return logList.size();
        }

        return 0;
    }

    @Override
    public void onClickRecentItem(final String id, final String title, String subtitle, String country) {
        Call<List<Content>> call = APIManager.getInstance().getApiCall().getContentDetailList(subtitle, country);
        BaseApplication.getInstance().progressOn(activity, "로딩중..", call);

        call.enqueue(new Callback<List<Content>>() {
            @Override
            public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
                List<Content> contents = response.body();

                if(contents.size() != 1) {
                    //MoveManager.getInstance().moveContentListPage(activity, title, contents);
                    MoveManager.getInstance().moveRecentToDetail(true, id, activity, title, contents);
                } else {
                    MoveManager.getInstance().moveContentDetailPage(activity, title, contents);
                }
            }

            @Override
            public void onFailure(Call<List<Content>> call, Throwable t) {
                call.clone().enqueue(this);
            }
        });

        /*Call<List<Content>> call = APIManager.getInstance().getApiCall().getContentDetailList(subtitle, country);
        BaseApplication.getInstance().progressOn(activity, "로딩중..", call);

        call.enqueue(new Callback<List<Content>>() {
            @Override
            public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
                List<Content> contents = response.body();
                //MoveManager.getInstance().moveContentDetailPage(activity, title, contents);

            }

            @Override
            public void onFailure(Call<List<Content>> call, Throwable t) {
                call.clone().enqueue(this);
            }
        });*/
    }

    @Override
    public void onClickClear(final int index, String logId) {
        DialogManager.getInstance().showDialogOkeyCancle(this.activity, index, R.string.ask_remove_record, this);
    }

    // 시청 기록을 삭제 하시겠습니까? - 확인
    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        int index = (Integer)dialog.getTag();
        APIManager.getInstance().getApiCall().deleteVideoLog(logList.get(index).logId).enqueue(new BasicResultCallback(index, this));
    }

    public void removeItem(int index) {
        logList.remove(index);
        notifyItemRemoved(index);

        if(logList.size() == 0) {
            TextView tv = activity.findViewById(R.id.empty_message);
            tv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void setOnClickListener(View.OnClickListener clickListener) {

    }

    @Override
    public void notifyAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public void addItems(List<VideoLog> list) {
        this.logList = list;
    }

    @Override
    public void clearItem() {

    }

    @Override
    public String getItem(int position) {
        return null;
    }
}
