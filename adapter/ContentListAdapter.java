package com.song.solartube.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.song.solartube.R;
import com.song.solartube.adapter.holder.ContentListHolder;
import com.song.solartube.listener.OnClickContentListListener;
import com.song.solartube.singleton.MoveManager;

/**
 * Created by Song on 2018-01-04.
 */

public class ContentListAdapter extends RecyclerView.Adapter<ContentListHolder> implements OnClickContentListListener{

    private Bundle bundle;
    private String[] ids;
    private String[] titles;
    private int[] parts;
    private Context context;

    public ContentListAdapter(Context context, Bundle bundle) {
        this.bundle = bundle;
        this.context = context;

        if(bundle != null) {
            this.ids = new String[bundle.getStringArray("ids").length];
            this.ids = bundle.getStringArray("ids");
            this.titles = new String[bundle.getStringArray("titles").length];
            this.titles = bundle.getStringArray("titles");
            this.parts = new int[bundle.getIntArray("parts").length];
            this.parts = bundle.getIntArray("parts");
        }
    }

    @Override
    public ContentListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        LinearLayout linearLayout = (LinearLayout)inflater
                .inflate(R.layout.holder_content_list, parent, false);

        ContentListHolder contentListHolder = new ContentListHolder(linearLayout, this);
        return contentListHolder;
    }

    @Override
    public void onBindViewHolder(ContentListHolder holder, int position) {
        holder.setData(ids[position]);
    }

    @Override
    public int getItemCount() {
        return this.ids.length;
    }

    @Override
    public void onClickContentList(String id, int part) {
        MoveManager.getInstance().moveSingleContentPage(this.context, id, part);
    }
}
