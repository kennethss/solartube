package com.song.solartube.adapter.contract;

import android.content.Context;

import com.song.solartube.callback.RecentContentCallback;

/**
 * Created by Song on 2018-01-17.
 */

public interface RecentContract {
    interface View {

        void showToast(String title);
    }

    interface Presenter {

        void attachView(View view);

        void detachView();

        void setRecentAdapterModel(RecentAdatperContract.Model adapterModel);

        void setRecentAdapterView(RecentAdatperContract.View adapterView);

        void setRecentContentCallback(RecentContentCallback recentContentCallback);

        //void setSampleImageData(SampleImageRepository sampleImageData);

        void loadItems(Context context, boolean isClear);
    }
}
