package com.song.solartube.adapter.contract;

import com.song.solartube.callback.RecentContentCallback;
import com.song.solartube.model.Content;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Song on 2018-01-18.
 */

public interface ContentThumbnailContract {
    interface View {
        void showToast(String title);

        void showProgress(String title, Call<List<Content>> call);

        void moveListPage(String title, List<Content> contents);

        void moveDetailPage(String title, List<Content> contents);
    }

    interface Presenter {

        void attachView(ContentThumbnailContract.View view);

        void detachView();

        void setContentThumbnailAdapterModel(ContentThumbnailAdapterContract.Model adapterModel);

        void setContentThumbnailAdapterView(ContentThumbnailAdapterContract.View adapterView);

        void setRecentContentCallback(RecentContentCallback recentContentCallback);

        //void setSampleImageData(SampleImageRepository sampleImageData);

        void loadMoreItems(int category, int size);
    }
}
