package com.song.solartube.adapter.contract;

import com.song.solartube.model.VideoLog;

import java.util.List;

/**
 * Created by Song on 2018-01-17.
 */

public interface RecentAdatperContract {

    interface View {

        void setOnClickListener(android.view.View.OnClickListener clickListener);

        void notifyAdapter();
    }

    interface Model {

        void addItems(List<VideoLog> imageItems);

        void clearItem();

        String getItem(int position);
    }
}
