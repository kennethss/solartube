package com.song.solartube.adapter.contract;

import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.ContentThumbnail;
import com.song.solartube.model.VideoLog;

import java.util.List;

/**
 * Created by Song on 2018-01-18.
 */

public interface ContentThumbnailAdapterContract {
    interface View {

        void setOnClickListener(OnClickThumbnailListener onClickThumbnailListener);

        void notifyAdapter();
    }

    interface Model {

        void addItems(List<ContentThumbnail> items);

        void clearItem();

        String getItem(int position);
    }
}
