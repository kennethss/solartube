package com.song.solartube.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.song.solartube.Define;
import com.song.solartube.R;
import com.song.solartube.listener.OnClickContentListListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.ThumbnailModel;
import com.song.solartube.util.StringUtil;

/**
 * Created by Song on 2018-01-04.
 */

public class ContentListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private String id;
    private int partNumber;

    private TextView title;
    private TextView part;
    private TextView date;
    private OnClickContentListListener listener;

    public ContentListHolder(View itemView, OnClickContentListListener listener) {
        super(itemView);

        this.listener = listener;

        title = itemView.findViewById(R.id.content_list_text);
        part = itemView.findViewById(R.id.content_list_part);
        date = itemView.findViewById(R.id.content_list_date);

        itemView.setOnClickListener(this);
    }

    public void setData(String id) {
        Content c = ThumbnailModel.getInstance().getContentDetail(id);

        this.id = id;
        this.partNumber = c.part;

        this.title.setText(c.title);

        String part = null;

        if(c.part < 10000) {
            if(c.part > 99) {
                part = String.valueOf(c.part);
                this.part.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            } else {
                part = StringUtil.append(String.valueOf(c.part), "화");
            }
        } else {
            part = StringUtil.append(String.valueOf(c.part - Define.SPECIAL_CODE), "화");
        }

        this.part.setText(part);
        this.date.setText(c.contentDate);
    }

    @Override
    public void onClick(View view) {
        listener.onClickContentList(this.id, this.partNumber);
    }
}
