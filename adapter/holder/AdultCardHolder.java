package com.song.solartube.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.song.solartube.R;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.ContentThumbnail;
import com.song.solartube.util.StringUtil;

import java.util.List;

/**
 * Created by Song on 2017-11-09.
 */

public class AdultCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private OnClickThumbnailListener onClickThumbnailListener;
    private ContentThumbnail contentThumbnail;

    private FlexboxLayout tagLayout;
    private LayoutInflater inflater;

    private TextView title;
    private TextView actor;

    public ImageView imageView;


    public AdultCardHolder(View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.holder_adult_cardView_image);
        title = itemView.findViewById(R.id.holder_adult_cardView_text);
        actor = itemView.findViewById(R.id.holder_adult_cardView_actor);
        tagLayout = itemView.findViewById(R.id.holder_adult_cardView_tag);

        itemView.setOnClickListener(this);
        this.inflater = LayoutInflater.from(itemView.getContext());
    }

    public void setData(OnClickThumbnailListener onClickThumbnailListener,
                             ContentThumbnail contentThumbnail) {

        this.onClickThumbnailListener = onClickThumbnailListener;
        this.contentThumbnail = contentThumbnail;

        title.setText(contentThumbnail.title);
        actor.setText(contentThumbnail.actor);
        String[] tags = contentThumbnail.tag.split(",");

        for (String tag : tags) {
            RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.tag_layout, null);

            TextView tv = view.findViewById(R.id.tag_tv);
            tv.setText(StringUtil.append("#", tag));

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(20, 20, 20, 20);

            tagLayout.addView(view, layoutParams);
        }
    }

    @Override
    public void onClick(View view) {
        onClickThumbnailListener.onClickThumbnail(contentThumbnail);
    }
}
