package com.song.solartube.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.song.solartube.R;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.ContentThumbnail;

/**
 * Created by Song on 2017-11-08.
 */

public class AdultHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private OnClickThumbnailListener onClickThumbnailListener;
    private ContentThumbnail contentThumbnail;

    public TextView title;

    public AdultHolder(View itemView) {
        super(itemView);

        title = itemView.findViewById(R.id.adult_text);
        itemView.setOnClickListener(this);
    }

    public void setThumbnail(OnClickThumbnailListener onClickThumbnailListener,
                             ContentThumbnail contentThumbnail) {
        this.onClickThumbnailListener = onClickThumbnailListener;
        this.contentThumbnail = contentThumbnail;
        title.setText(contentThumbnail.title);
    }

    @Override
    public void onClick(View view) {
        Log.d("정보", contentThumbnail.contentId + contentThumbnail.title + contentThumbnail.country);
        onClickThumbnailListener.onClickThumbnail(contentThumbnail);
    }
}
