package com.song.solartube.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.util.Util;
import com.song.solartube.R;
import com.song.solartube.listener.OnClickRecentThumbnailListener;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.VideoLog;
import com.song.solartube.util.StringUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

/**
 * Created by Song on 2017-11-21.
 */

public class RecentContentHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

    private ImageView thumbnail;
    private ImageView clear;
    private TextView titleView;
    private TextView date;
    private TextView position;
    private OnClickRecentThumbnailListener listener;
    private VideoLog log;

    private StringBuilder formatBuilder = new StringBuilder();
    private Formatter formatter = new Formatter(formatBuilder, Locale.getDefault());

    private String getPosition(long time, long contentPosition) {
        return StringUtil.append(
                Util.getStringForTime(formatBuilder, formatter, time),
                " / ",
                Util.getStringForTime(formatBuilder, formatter, contentPosition)
        );
    }

    private String getDate(String dateString) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = null;

        try {
            date = fmt.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return fmtOut.format(date);
    }

    public RecentContentHolder(View itemView) {
        super(itemView);

        thumbnail = itemView.findViewById(R.id.recent_thumbnail);
        clear = itemView.findViewById(R.id.recent_clear);
        titleView = itemView.findViewById(R.id.recent_title);
        date = itemView.findViewById(R.id.recent_date);
        position = itemView.findViewById(R.id.recent_position);

        itemView.setOnClickListener(this);
        clear.setOnClickListener(this);
    }

    public void setData(VideoLog log, OnClickRecentThumbnailListener listener) {

        String position = getPosition(log.time, log.contentPosition);
        String date = StringUtil.append("시청일", " : ", getDate(log.saveTime));

        this.titleView.setText(log.title);
        this.date.setText(date);
        this.position.setText(position);
        this.log = log;
        this.listener= listener;
    }

    public ImageView getThumbnailView() {
        return thumbnail;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == -1) {
            this.listener.onClickRecentItem(log.contentId, log.title, log.subtitle, log.country);
        } else if (id == R.id.recent_clear) {
            this.listener.onClickClear(getAdapterPosition(), log.logId);
        }
    }

    @Override
    public boolean onLongClick(View view) {

        return false;
    }
}
