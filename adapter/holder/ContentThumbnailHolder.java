package com.song.solartube.adapter.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.song.solartube.R;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.ContentThumbnail;

/**
 * Created by Song on 2017-09-12.
 */

public class ContentThumbnailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView posterTitle;
    private ImageView posterImage;
    private ImageView posterAdultMark;

    private ContentThumbnail contentThumbnail;
    private OnClickThumbnailListener onClickThumbnailListener;

    public ContentThumbnailHolder(View itemView) {
        super(itemView);

        posterImage = itemView.findViewById(R.id.poster_image);
        posterTitle = itemView.findViewById(R.id.poster_title);
        posterAdultMark = itemView.findViewById(R.id.poster_adult_mark);

        itemView.setOnClickListener(this);
    }

    public void setData(OnClickThumbnailListener onClickThumbnailListener, ContentThumbnail contentThumbnail) {
        this.onClickThumbnailListener = onClickThumbnailListener;
        this.contentThumbnail = contentThumbnail;
        posterTitle.setText(contentThumbnail.title);

        if(contentThumbnail.rating >= 19) {
            posterAdultMark.setVisibility(View.VISIBLE);
        } else {
            posterAdultMark.setVisibility(View.GONE);
        }
    }

    public ImageView getHolderImageView() {
        return posterImage;
    }

    @Override
    public void onClick(View view) {
        onClickThumbnailListener.onClickThumbnail(this.contentThumbnail);
    }
}
