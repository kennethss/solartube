package com.song.solartube;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.song.solartube.adapter.ContentThumbnailAdapter;

/**
 * Created by Song on 2017-12-26.
 */

public class ThumbnailSpanSizeLookup extends GridLayoutManager.SpanSizeLookup{
    private ContentThumbnailAdapter adapter;

    public ThumbnailSpanSizeLookup(ContentThumbnailAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public int getSpanSize(int position) {
        if(adapter.getItemCount() == position + 1){
            if(adapter.getIsLast()) {
                return 1;
            } else {
                return 3;
            }

        } else {
            return 1;
        }
    }
}
