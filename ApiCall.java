package com.song.solartube;

import com.song.solartube.model.Content;
import com.song.solartube.model.ContentThumbnail;
import com.song.solartube.model.Result;
import com.song.solartube.model.User;
import com.song.solartube.model.SearchData;
import com.song.solartube.model.UserData;
import com.song.solartube.model.Version;
import com.song.solartube.model.VideoLog;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Song on 2017-09-05.
 */

public interface ApiCall {

    // GET URL 자체에 Param
    Call<Content> getContentInfo(@Path("version") int version, @Path("uuid") String uuid);

    // GET - READ
    @GET("/CI/api/API/getMovieList")
    Call<List<ContentThumbnail>> getMovieList();

    @GET("/CI/api/API/getKoreaMovieList")
    Call<List<ContentThumbnail>> getKoreaMovieList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getForeignMovieList")
    Call<List<ContentThumbnail>> getForeignMovieList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getKoreaDramaList")
    Call<List<ContentThumbnail>> getKoreaDramaList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getForeignDramaList")
    Call<List<ContentThumbnail>> getForeignDramaList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getDramaList")
    Call<List<ContentThumbnail>> getDramaList();

    @GET("/CI/api/API/getAnimationList")
    Call<List<ContentThumbnail>> getAnimationList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getEntertainmentList")
    Call<List<ContentThumbnail>> getEntertainmentList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getDocumentaryList")
    Call<List<ContentThumbnail>> getDocumentaryList(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/CI/api/API/getContentAllSearchList")
    Call<List<SearchData>> getContentAllSearchList();

    @GET("/CI/api/API/getContentDetail")
    Call<Content> getContentDetail(@Query("id") String id);

    @GET("/CI/api/API/getContentDetailList")
    Call<List<Content>> getContentDetailList(@Query("subtitle") String subtitle, @Query("country") String country);

    @GET("/CI/api/API/getSearchContentDetailList")
    Call<List<Content>> getSearchContentDetailList(@Query("title") String subtitle);

    @GET("/CI/api/API/getAdultKoreaList")
    Call<List<ContentThumbnail>> getAdultKoreaList();

    @GET("/CI/api/API/getAdultJapanList")
    Call<List<ContentThumbnail>> getAdultJapanList();

    @GET("/CI/api/API/getAdultJapanAvList")
    Call<List<ContentThumbnail>> getAdultJapanAvList();

    @GET("/CI/api/API/getAdultContentDetailList")
    Call<List<Content>> getAdultContentDetailList(@Query("title") String title, @Query("country") String country);

    @GET("/CI/api/API/getVideoLog")
    Call<VideoLog> getVideoLog(@Query("content_id") String contentId, @Query("user_id") int userId);

    @GET("/CI/api/API/getRecentVideoContentLog")
    Call<List<VideoLog>> getRecentVideoContentLog(@Query("user_id") int userId);

    @GET("/CI/api/API/getLastVersion")
    Call<Version> getLastVersion();

    @GET("http://wlsrhkd4023.iptime.org:10080/SolarTube/Resources/Apk/SolarTube.apk")
    Call<ResponseBody> downloadFileWithFixedUrl();

    //@GET("/CI/api/API/deleteVideoLog")
    //Call<User> deleteVideoLog(@Query("log_id") String logId);

    // POST - CREATE
    @FormUrlEncoded
    @POST("/CI/api/API/registerSolarTube")
    Call<User> registerSolarTube(@Field("id") String id, @Field("name") String name, @Field("type") String type);

    @FormUrlEncoded
    @POST("/CI/api/API/addRequestContent")
    Call<Result> addRequestContent(@Field("user_id") int userId, @Field("category") String category, @Field("text") String text);

    @FormUrlEncoded
    @POST("/CI/api/API/addVideoLog")
    Call<User> addVideoLog(@Field("content_id") String contentId, @Field("thumbnail") String thumbnail,
                           @Field("title") String title, @Field("subtitle") String subtitle,
                           @Field("country") String country, @Field("user_id") int userId,
                           @Field("time") int time, @Field("contentPosition") int contentPosition);

    @FormUrlEncoded
    @POST("/CI/api/API/requestModify")
    Call<Result> requestModify(@Field("id") String id, @Field("user_id") int userId, @Field("time") String time, @Field("text") String text);

    @FormUrlEncoded
    @POST("update")
    Call<Content> postUpdateContent(@Field("test_a") String test_a, @Field("test_b") String test_B);

    @FormUrlEncoded
    @POST("/CI/api/API/validateUser")
    Call<UserData> validateUser(@Field("type") String type, @Field("id") String id);


    // PUT - UPDATE/REPLACE
    @PUT("/CI/api/API/updateVideoLog")
    Call<User> updateVideoLog(@Query("log_id") String logId, @Query("time") int time, @Query("savetime") String saveTime);

    @PUT("/CI/api/API/registerAdmissionCode")
    Call<Result> registerAdmissionCode(@Query("code") String code);

    // DELETE - DELETE
    @DELETE("/CI/api/API/deleteVideoLog/{log_id}")
    Call<Result> deleteVideoLog(@Path("log_id") String logId);
}

