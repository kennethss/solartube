package com.song.solartube.model;

import com.song.solartube.singleton.APIManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-11-10.
 */

public class ContentThumbnailModel implements Callback<List<ContentThumbnail>> {

    private final int LIMIT = 10;
    private ContentThumbnailCallback callback;

    public ContentThumbnailModel(ContentThumbnailCallback callback) {
        this.callback = callback;
    }


    public void request(ContentThumbnailCallback callback, int category, int size) {
        this.callback = callback;
        APIManager.getInstance().getApiType(category, LIMIT, size).enqueue(this);
    }

    public void requestDetailList(String subtitle, String country) {

    }

    @Override
    public void onResponse(Call<List<ContentThumbnail>> call, Response<List<ContentThumbnail>> response) {
        this.callback.getContentThumbnail(response.body());
    }

    @Override
    public void onFailure(Call<List<ContentThumbnail>> call, Throwable t) {
        call.clone().enqueue(this);
    }

    public interface ContentThumbnailCallback {
        void getContentThumbnail(List<ContentThumbnail> thumbnails);
    }
}
