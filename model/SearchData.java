package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-11-10.
 */

public class SearchData {
    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("content_id")
    public String contentId;

    @SerializedName("country")
    public String country;
}
