package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-11-07.
 */

public class UserData {
    @SerializedName("user_id")
    public String userId;

    @SerializedName("facebook_id")
    public int facebookId;

    @SerializedName("kakaotalk_id")
    public int kakaoTalkId;

    @SerializedName("age")
    public int age;
}
