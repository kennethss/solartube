package com.song.solartube.model;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-09-13.
 */

public class ThumbnailModel implements Callback<List<ContentThumbnail>>{

    private HashMap<String, Content> contentHashMap = new HashMap<>();
    private static ThumbnailModel instance;

    public static ThumbnailModel getInstance() {

        if (instance == null) {
            instance = new ThumbnailModel();
        }

        return instance;
    }

    public Content getContentDetail(String id) {
        return contentHashMap.get(id);
    }

    public void addContentDetail(String key, Content content) {
        contentHashMap.put(key, content);
    }

    @Override
    public void onResponse(Call<List<ContentThumbnail>> call, Response<List<ContentThumbnail>> response) {

    }

    @Override
    public void onFailure(Call<List<ContentThumbnail>> call, Throwable t) {

    }
}

