package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-12-07.
 */

public class Version {
    @SerializedName("version_code")
    public int versionCode;

    @SerializedName("update_text")
    public String updateText;
}

