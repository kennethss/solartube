package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-09-05.
 */

public class Content {

    @SerializedName("content_id")
    public String contentId;

    @SerializedName("part")
    public int part;

    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("content_date")
    public String contentDate;

    @SerializedName("genre")
    public String genre;

    @SerializedName("length")
    public String videoLength;

    @SerializedName("country")
    public String country;

    @SerializedName("director")
    public String director;

    @SerializedName("description")
    public String description;

    @SerializedName("actor")
    public String actor;

    @SerializedName("rating")
    public String rating;

    @SerializedName("path")
    public String path;

    @SerializedName("poster")
    public String poster;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("filename")
    public String filename;

    @SerializedName("upload_date")
    public String uploadDate;

    @SerializedName("size")
    public String size;

    @SerializedName("comment_id")
    public String commentId;

    @SerializedName("isSubTitle")
    public int isSubTitle;
}
