package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-11-20.
 */

public class User {
    @SerializedName("user_id")
    public int userId;

    @SerializedName("name")
    public String name;

    @SerializedName("type")
    public String type;

    @SerializedName("age")
    public int age;
}
