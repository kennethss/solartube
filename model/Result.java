package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-12-06.
 */

public class Result {
    @SerializedName("resultCode")
    public int resultCode;

    @SerializedName("message")
    public String message;
}
