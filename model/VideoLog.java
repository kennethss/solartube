package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-11-20.
 */

public class VideoLog {

    @SerializedName("log_id")
    public String logId;

    @SerializedName("content_id")
    public String contentId;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("country")
    public String country;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("time")
    public long time;

    @SerializedName("contentPosition")
    public long contentPosition;

    @SerializedName("savetime")
    public String saveTime;
}
