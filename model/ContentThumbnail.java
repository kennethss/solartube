package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-09-13.
 */

public class ContentThumbnail {
    @SerializedName("content_id")
    public String contentId;

    @SerializedName("title")
    public String title;

    @SerializedName("subtitle")
    public String subtitle;

    @SerializedName("actor")
    public String actor;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("tag")
    public String tag;

    @SerializedName("content_date")
    public String contentDate;

    @SerializedName("rating")
    public int rating;

    @SerializedName("genre")
    public String genre;

    @SerializedName("country")
    public String country;
}
