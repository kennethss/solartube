package com.song.solartube.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Song on 2017-09-13.
 */

public class Movie {
    @SerializedName("content_id")
    public String contentId;

    @SerializedName("original_name")
    public String originalName;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("content_date")
    public String contentDate;

    @SerializedName("upload_date")
    public String uploadDate;

    @SerializedName("video_length")
    public String videoLength;

    @SerializedName("rating")
    public String rating;

    @SerializedName("appearance")
    public String appearance;

    @SerializedName("size")
    public String size;

    @SerializedName("comment_id")
    public String commentId;

    @SerializedName("path")
    public String path;
}
