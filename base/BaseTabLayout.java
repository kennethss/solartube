package com.song.solartube.base;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.song.solartube.R;

/**
 * Created by Song on 2017-11-16.
 */

public class BaseTabLayout extends TabLayout{
    private Typeface mTypeface;

    public BaseTabLayout(Context context) {
        super(context);
        init();
    }

    public BaseTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/NanumBarunGothic.ttf"); // here you will provide fully qualified path for fonts
        //mTypeface = getResources().getFont(R.font)
    }

    @Override
    public void addTab(@NonNull Tab tab) {
        super.addTab(tab);

        Log.d("셋업,", "addTab");

        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();

        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
            if (tabViewChild instanceof TextView) {
                ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
            }
        }
    }

    @Override
    public void setupWithViewPager(ViewPager viewPager) {
        super.setupWithViewPager(viewPager);

        Log.d("셋업,", "setupWithViewPager");

        for (int i = 0; i < getTabCount(); i++) {
            //R.layout is the previous defined xml
            TextView tv=(TextView) LayoutInflater.from(getContext()).inflate(R.layout.textview,null);
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/NanumBarunGothic.ttf"); // here you will provide fully qualified path for fonts
            tv.setTypeface(typeface);
            getTabAt(i).setCustomView(tv);
        }

        //changeTabsFont();

        /*if (mTypeface != null) {
            this.removeAllTabs();
            ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);

            PagerAdapter adapter = viewPager.getAdapter();

            for (int i = 0, count = adapter.getCount(); i < count; i++) {
                Tab tab = this.newTab();
                this.addTab(tab.setText(adapter.getPageTitle(i)));
                AppCompatTextView view = (AppCompatTextView) ((ViewGroup) slidingTabStrip.getChildAt(i)).getChildAt(1);
                view.setTypeface(mTypeface, Typeface.BOLD);
            }
        }*/
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) this.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
                }
            }
        }
    }
}
