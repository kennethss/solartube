package com.song.solartube.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Song on 2017-11-08.
 */

public class BaseFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<Fragment> list;
    String[] titles;

    public BaseFragmentStatePagerAdapter(FragmentManager fm, int count, ArrayList<Fragment> list, String[] titles) {
        super(fm);

        this.list = list;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
