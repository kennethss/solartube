package com.song.solartube.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.song.solartube.model.Content;

import java.util.List;

import retrofit2.Call;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Song on 2017-09-07.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase){
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void progressOn() {
        BaseApplication.getInstance().progressOn(this, null, null);
    }

    public void progressOn(String message) {
        BaseApplication.getInstance().progressOn(this, message , null);
    }

    public void progressOn(String message, Call<List<Content>> call) {
        BaseApplication.getInstance().progressOn(this, message, call);
    }

    public void progressOFF() {
        BaseApplication.getInstance().progressOFF();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
