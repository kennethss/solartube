package com.song.solartube.base;

import android.app.Activity;
import android.app.Application;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.widget.TextView;

import com.solar.sns.SocialManager;
import com.song.solartube.R;
import com.song.solartube.model.Content;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.MoveManager;

import java.util.List;

import retrofit2.Call;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Song on 2017-11-06.
 */

public class BaseApplication extends Application implements DialogInterface.OnCancelListener{

    private static BaseApplication baseApplication;
    AppCompatDialog progressDialog;

    public static BaseApplication getInstance() {
        return baseApplication;
    }
    Call<List<Content>> call;

    @Override
    public void onCreate() {
        super.onCreate();
        baseApplication = this;

        initFont();
        SocialManager.getInstance().initSocial(this);
        initSingleton();
    }

    private void initSingleton() {
        APIManager.getInstance();
        MoveManager.getInstance();
    }

    private void initFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/NanumBarunGothic.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    public void progressOn(Activity activity, String message, Call<List<Content>> call) {
        if (activity == null || activity.isFinishing()) {
            return;
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            progressSET(message);
        } else {
            progressDialog = new AppCompatDialog(activity);
            progressDialog.setOnCancelListener(this);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.activity_loading);
            progressDialog.show();

            if (call != null) {
                this.call = call;
            }
        }
    }

    public void progressSET(String message) {
        if (progressDialog == null || !progressDialog.isShowing()) {
            return;
        }

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }
    }

    public void progressOFF() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        this.call.cancel();
    }
}
