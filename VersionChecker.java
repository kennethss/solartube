package com.song.solartube;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.song.solartube.model.Version;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.util.Preference;
import com.song.solartube.util.TimeController;
import com.song.solartube.util.VersionController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-07.
 */

public class VersionChecker implements Callback<Version>, SingleButtonCallback, OnCheckedChangeListener, DialogInterface.OnDismissListener{

    private Context context;
    private String updateText;

    private boolean dailyCheck;
    private Preference preference;
    private final String PREFERENCE_KEY = "SolarTube";

    private void closedDialog() {
        TimeController t = new TimeController();

        if (dailyCheck) {
            preference.saveCurrentTime(context, PREFERENCE_KEY, t.getCurrentTime());
        }
    }

    private boolean writeResponseBodyToDisk(ResponseBody body) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(context.getExternalFilesDir(null) + File.separator + "SolarTube.apk");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d("VersionChecker", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                File toInstall = new File(context.getExternalFilesDir(null) + File.separator + "SolarTube.apk");
                VersionController v = new VersionController(context);
                v.callAppInstallActivity(toInstall);

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }


    }

    private void update() {
        APIManager.getInstance().getApiCall().downloadFileWithFixedUrl().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    Log.d("VersionChecker", "server contacted and has file");

                    boolean writtenToDisk = writeResponseBodyToDisk(response.body());

                    Log.d("VersionChecker", "file download was a success? " + writtenToDisk);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public VersionChecker (Context context) {
        this.context = context;
        this.preference = new Preference();
    }

    public void versionCheck() {
        TimeController t = new TimeController();

        long lastTime = preference.getLastTime(context, PREFERENCE_KEY);

        if (lastTime == 0) {
            APIManager.getInstance().getApiCall().getLastVersion().enqueue(this);
        } else {
            long currentTime = t.getCurrentTime();
            long day = Math.abs((currentTime - lastTime) / (24 * 60 * 60 * 1000));

            if (day > 0) {
                APIManager.getInstance().getApiCall().getLastVersion().enqueue(this);
            }
        }
    }

    @Override
    public void onResponse(Call<Version> call, Response<Version> response) {
        PackageInfo info = null;

        try {
             info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (response.isSuccessful()) {
            Version version = response.body();

            if (info.versionCode < version.versionCode) {
                new MaterialDialog.Builder(context)
                        .title(R.string.ask_new_version)
                        .content(version.updateText)
                        .positiveColor(context.getResources().getColor(R.color.colorBlack))
                        .negativeColor(context.getResources().getColor(R.color.colorBlack))
                        .positiveText(R.string.agree)
                        .negativeText(R.string.cancel)
                        .widgetColor(Color.BLACK)
                        .checkBoxPromptRes(R.string.do_not_see_today, false, this)
                        .onPositive(this)
                        .onNegative(this)
                        .onNeutral(this)
                        .autoDismiss(true)
                        .dismissListener(this)
                        .show();
            }
        }
    }

    @Override
    public void onFailure(Call<Version> call, Throwable t) {
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        dailyCheck = b;
    }

    // Ok Listener
    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which) {
            case POSITIVE:
                update();
                break;
            case NEGATIVE:
                break;
            case NEUTRAL:
                break;
        }

        closedDialog();
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        closedDialog();
    }
}
