package com.song.solartube;

import android.app.Activity;

import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.song.solartube.player.CustomPlayerController;

/**
 * Created by Song on 2018-01-05.
 */

public class ContentDetailDetector implements GestureDetector.OnGestureListener {
    private GestureDetectorCompat mDetector;
    private CustomPlayerController customPlayerController;

    public ContentDetailDetector(CustomPlayerController customPlayerController, Activity activity) {
        this.customPlayerController = customPlayerController;
        mDetector = new GestureDetectorCompat(activity, this);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        Log.d("스크롤~", "스크롤~");
        customPlayerController.onScroll(motionEvent, motionEvent1, v, v1);
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }
}
