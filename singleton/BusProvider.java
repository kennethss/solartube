package com.song.solartube.singleton;

import com.squareup.otto.Bus;

/**
 * Created by Song on 2017-11-20.
 */

public final class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {

    }


}
