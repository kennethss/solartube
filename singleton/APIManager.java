package com.song.solartube.singleton;

import android.content.pm.ApplicationInfo;

import com.song.solartube.ApiCall;
import com.song.solartube.DefaultRestClient;
import com.song.solartube.Define;
import com.song.solartube.model.ContentThumbnail;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Song on 2017-11-10.
 */

public class APIManager {
    private static APIManager instance;

    private ApiCall apiCall;
    private DefaultRestClient<ApiCall> defaultRestClient;

    private APIManager() {
        defaultRestClient = new DefaultRestClient<>();
        apiCall = defaultRestClient.getClient(ApiCall.class);
    }

    public static APIManager getInstance() {
        if (instance == null) {
            instance = new APIManager();
        }

        return instance;
    }

    public ApiCall getApiCall() {
        return apiCall;
    }

    public Call<List<ContentThumbnail>> getApiType(int category, int limit, int offset) {
        switch (category) {
            case Define.HOME:
                return apiCall.getForeignMovieList(limit, offset);
            case Define.MOVIE_KOREA:
                return apiCall.getKoreaMovieList(limit, offset);
            case Define.MOVIE_FOREIGN:
                return apiCall.getForeignMovieList(limit, offset);
            case Define.DRAMA_KOREA:
                return apiCall.getKoreaDramaList(limit, offset);
            case Define.DRAMA_FOREIGN:
                return apiCall.getForeignDramaList(limit, offset);
            case Define.ENTERTAINMENT:
                return apiCall.getEntertainmentList(limit, offset);
            case Define.DOCUMENTARY:
                return apiCall.getDocumentaryList(limit, offset);
            case Define.ANIMATION:
                return apiCall.getAnimationList(limit, offset);
        }
        return null;
    }
}
