package com.song.solartube.singleton;

import android.content.Context;
import android.content.Intent;

import com.song.solartube.activity.ContentDetailActivity;
import com.song.solartube.activity.ContentListActivity;
import com.song.solartube.base.BaseApplication;
import com.song.solartube.model.Content;
import com.song.solartube.model.ThumbnailModel;

import java.util.List;

/**
 * Created by Song on 2017-11-10.
 */

public class MoveManager {
    private static MoveManager instance;

    public static MoveManager getInstance() {
        if(instance == null) {
            instance = new MoveManager();
        }

        return instance;
    }

    public void moveSingleContentPage(Context context, String id, int part) {
        String[] ids = new String[]{id};

        Intent intent = new Intent(context, ContentDetailActivity.class);
        intent.putExtra("multiple", true);
        intent.putExtra("ids", ids);

        context.startActivity(intent);
    }

    public void moveRecentToDetail(boolean multiple, String id, Context context, String title, List<Content> contents) {
        /*String[] ids = new String[]{id};

        Intent intent = new Intent(context, ContentListActivity.class);
        intent.putExtra("multiple", true);
        intent.putExtra("ids", ids);
        context.startActivity(intent);*/

        String[] ids = new String[contents.size()];
        String[] titles = new String[contents.size()];
        int[] parts = new int[contents.size()];

        for (int i=0; i<contents.size(); i++) {
            String contentId = contents.get(i).contentId;
            int part = contents.get(i).part;
            ids[i] = contentId;
            parts[i] = part;
            titles[i] = contents.get(i).title;

            ThumbnailModel.getInstance().addContentDetail(contentId, contents.get(i));
        }

        Intent intent = new Intent(context, ContentListActivity.class);
        intent.putExtra("parts", parts);
        intent.putExtra("ids", ids);
        intent.putExtra("id", id);
        intent.putExtra("titles", titles);
        intent.putExtra("title", title);
        intent.putExtra("multiple", multiple);
        context.startActivity(intent);

        BaseApplication.getInstance().progressOFF();
    }

    public void moveContentDetailPage(final Context context, String title, List<Content> contents) {

        String[] ids = new String[contents.size()];
        String[] titles = new String[contents.size()];
        int[] parts = new int[contents.size()];

        for (int i=0; i<contents.size(); i++) {
            String id = contents.get(i).contentId;
            int part = contents.get(i).part;
            ids[i] = id;
            parts[i] = part;
            titles[i] = contents.get(i).title;

            ThumbnailModel.getInstance().addContentDetail(id, contents.get(i));
        }

        Intent intent = new Intent(context, ContentDetailActivity.class);
        intent.putExtra("parts", parts);
        intent.putExtra("ids", ids);
        intent.putExtra("titles", titles);
        intent.putExtra("title", title);
        context.startActivity(intent);

        BaseApplication.getInstance().progressOFF();
    }

    public void moveContentListPage(final Context context, String title, List<Content> contents) {

        String[] ids = new String[contents.size()];
        String[] titles = new String[contents.size()];
        int[] parts = new int[contents.size()];

        for (int i=0; i<contents.size(); i++) {
            String id = contents.get(i).contentId;
            int part = contents.get(i).part;
            ids[i] = id;
            parts[i] = part;
            titles[i] = contents.get(i).title;

            ThumbnailModel.getInstance().addContentDetail(id, contents.get(i));
        }

        Intent intent = new Intent(context, ContentListActivity.class);
        intent.putExtra("parts", parts);
        intent.putExtra("ids", ids);
        intent.putExtra("titles", titles);
        intent.putExtra("title", title);
        context.startActivity(intent);

        BaseApplication.getInstance().progressOFF();
    }

    public void startActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }
}
