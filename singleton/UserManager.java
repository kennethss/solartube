package com.song.solartube.singleton;

import com.song.solartube.model.User;

/**
 * Created by Song on 2017-11-20.
 */

public class UserManager {
    private static UserManager instance;
    private User user = null;

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }

        return instance;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAdult() {
        if(user.age == 19) {
            return true;
        }

        return false;
    }

    public boolean isPremiumAdult() {
        if(user.age == 29) {
            return true;
        }

        return false;
    }

    public User getUser() {
        return this.user;
    }

    public void clearUserData() {
        this.user = null;
    }

    public boolean isLogin() {
        if (user == null) {
            return false;
        }

        return true;
    }
}
