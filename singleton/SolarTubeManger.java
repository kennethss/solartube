package com.song.solartube.singleton;

import android.content.Context;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.solar.sns.LoginActivity;
import com.song.solartube.R;
import com.song.solartube.activity.RecentContentActivity;

/**
 * Created by Song on 2017-12-04.
 */

public class SolarTubeManger {

    private static SolarTubeManger instance;

    public static SolarTubeManger getInstance() {
        if (instance == null) {
            instance = new SolarTubeManger();
        }

        return instance;
    }

    public void moveToLoginCheckAfterActivity(final Context context, Class<?> cls) {
        if (UserManager.getInstance().isLogin()) {
            MoveManager.getInstance().startActivity(context, cls);
        } else {
            DialogManager.getInstance().showDialogOkeyCancle(context, null, R.string.necessary_login, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    MoveManager.getInstance().startActivity(context, LoginActivity.class);
                }
            });
        }
    }
}