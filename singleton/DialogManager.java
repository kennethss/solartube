package com.song.solartube.singleton;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.song.solartube.R;


/**
 * Created by Song on 2017-12-04.
 */

public class DialogManager {
    private static DialogManager instance;

    public static DialogManager getInstance() {
        if (instance == null) {
            instance = new DialogManager();
        }

        return instance;
    }

    public void showDialogOkeyCancle(Context context, Object tag, int resId, SingleButtonCallback positiveCallback) {
        new MaterialDialog.Builder(context)
                .content(resId)
                .positiveColor(context.getResources().getColor(R.color.colorBlack))
                .negativeColor(context.getResources().getColor(R.color.colorBlack))
                .positiveText(R.string.agree)
                .negativeText(R.string.cancel)
                .onPositive(positiveCallback)
                .tag(tag)
                .show();
    }

    public void showDialogInputText(Context context, String title, SingleButtonCallback callback) {
        new MaterialDialog.Builder(context)
                .title(title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .widgetColor(context.getResources().getColor(R.color.colorRed))
                .inputRangeRes(0, 10, R.color.colorRed)
                .input("ex)33819237", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {

                    }
                })
                .alwaysCallInputCallback()
                .onPositive(callback)
                .positiveColor(context.getResources().getColor(R.color.colorBlack))
                .show();
    }

    public void showDefaultDialog(Context context, String title) {
        new MaterialDialog.Builder(context)
                .content(title)
                .positiveColor(context.getResources().getColor(R.color.colorBlack))
                .negativeColor(context.getResources().getColor(R.color.colorBlack))
                .positiveText(R.string.agree)
                .show();
    }
}

