package com.song.solartube;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Song on 2017-11-10.
 */

public class MySuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.song.solartube.MySuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public MySuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
