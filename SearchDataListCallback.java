package com.song.solartube;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.song.solartube.model.SearchData;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-11-20.
 */

public class SearchDataListCallback implements Callback<List<SearchData>> {

    private MaterialSearchView materialSearchView;
    private HashMap<String, SearchData> searchDataHashMap;

    public SearchDataListCallback(MaterialSearchView materialSearchView, HashMap<String, SearchData> searchDataHashMap) {
        this.materialSearchView = materialSearchView;
        this.searchDataHashMap = searchDataHashMap;
    }

    @Override
    public void onResponse(Call<List<SearchData>> call, Response<List<SearchData>> response) {
        List<SearchData> searchData = response.body();
        String[] searchList = new String[searchData.size()];

        for (int i = 0; i < searchData.size(); i++) {
            String title = searchData.get(i).title;
            searchDataHashMap.put(title, searchData.get(i));
            searchList[i] = title;
        }

        materialSearchView.setSuggestions(searchList);
    }

    @Override
    public void onFailure(Call<List<SearchData>> call, Throwable t) {
        call.clone().enqueue(this);
    }
}
