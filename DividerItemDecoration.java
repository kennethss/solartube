package com.song.solartube;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.song.solartube.adapter.RecentContentAdapter;

/**
 * Created by Song on 2017-11-22.
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable parentDivider;
    private Drawable subDivider;

    public DividerItemDecoration(Context context) {
        parentDivider = context.getDrawable(R.drawable.square);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);

        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            RecyclerView.ViewHolder holder = parent.getChildViewHolder(child);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;

        }
    }
}
