package com.song.solartube.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.song.solartube.ApiCall;
import com.song.solartube.R;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.adapter.AdultCardListAdapter;
import com.song.solartube.adapter.AdultListAdapter;
import com.song.solartube.Define;

/**
 * Created by Song on 2017-11-07.
 */

public class AdultFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        recyclerView = (RecyclerView) inflater.inflate(
                R.layout.fragment_horizontal, container, false);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(null, 1,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);

        return recyclerView;
    }

    public void initAdapter(String type) {
        ApiCall api = APIManager.getInstance().getApiCall();

        switch (type) {
            case Define.ADULT_KOREA:
                adapter = new AdultListAdapter(this, api.getAdultKoreaList());
                break;
            case Define.ADULT_JAPAN:
                adapter = new AdultCardListAdapter(this, api.getAdultJapanList());
                break;

            case Define.ADULT_JAPAN_AV:
                adapter = new AdultCardListAdapter(this, api.getAdultJapanAvList());
                break;
        }

    }
}
