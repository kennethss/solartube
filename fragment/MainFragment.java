package com.song.solartube.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.song.solartube.R;
import com.song.solartube.ThumbnailSpanSizeLookup;
import com.song.solartube.adapter.ContentThumbnailAdapter;
import com.song.solartube.base.BaseApplication;
import com.song.solartube.adapter.contract.ContentThumbnailContract;
import com.song.solartube.model.Content;
import com.song.solartube.presenter.ContentThumbnailPresenter;
import com.song.solartube.singleton.MoveManager;
import com.song.solartube.util.EndlessRecyclerOnScrollListener;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Song on 2017-09-12.
 */

public class MainFragment extends Fragment implements ContentThumbnailContract.View{

    int category;

    RecyclerView recyclerView;
    ContentThumbnailAdapter adapter;
    ContentThumbnailPresenter contentThumbnailPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        recyclerView = (RecyclerView) inflater
                .inflate(R.layout.fragment_horizontal, container, false);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(null, 3,
                LinearLayoutManager.VERTICAL, false);

        gridLayoutManager.setSpanSizeLookup(new ThumbnailSpanSizeLookup(adapter));

        EndlessRecyclerOnScrollListener a = new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                contentThumbnailPresenter.loadMoreItems(category, adapter.getThumbnailsCount());
            }
        };

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(a);

        return recyclerView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void initAdapter(int category) {
        this.category = category;

        adapter = new ContentThumbnailAdapter(this);
        contentThumbnailPresenter = new ContentThumbnailPresenter();
        contentThumbnailPresenter.setContentThumbnailAdapterModel(adapter);
        contentThumbnailPresenter.setContentThumbnailAdapterView(adapter);
        contentThumbnailPresenter.attachView(this);
    }

    @Override
    public void showToast(String title) {

    }

    @Override
    public void showProgress(String title, Call<List<Content>> call) {
        BaseApplication.getInstance().progressOn(getActivity(), title, call);
    }

    @Override
    public void moveListPage(String title, List<Content> contents) {
        MoveManager.getInstance().moveContentListPage(getContext(), title, contents);
    }

    @Override
    public void moveDetailPage(String title, List<Content> contents) {
        MoveManager.getInstance().moveContentDetailPage(getContext(), title, contents);
    }
}
