package com.song.solartube.listener;

import com.song.solartube.model.ContentThumbnail;

/**
 * Created by Song on 2017-09-13.
 */

public interface OnClickThumbnailListener {
    void onClickThumbnail(ContentThumbnail contentThumbnail);
}
