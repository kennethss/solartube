package com.song.solartube.listener;

/**
 * Created by Song on 2017-11-17.
 */

public interface CustomPlayerControllerListener {
    void onClickLandScape();

    void onClickControllerMore();

    void onClickArrowBack();

    void onClickResizeFit();

    void onClickResizeFill();

    void onClickSubtitleOption();

    void onClickModifyRequest();

    void onClickController();

    void onDragStart(boolean play);

    void onDragEnd(long position);

    void onShowBottomSheet();

    void onHideBottomSheet();

    void onHideUiOptions();
}
