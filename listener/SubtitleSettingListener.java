package com.song.solartube.listener;

/**
 * Created by Song on 2017-10-19.
 */

public interface SubtitleSettingListener {
    void onChangedTextSize(float size);
    void onChangedBottomPadding(float size);
}
