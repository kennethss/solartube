package com.song.solartube.listener;

/**
 * Created by Song on 2017-12-05.
 */

public interface OnClickRecentThumbnailListener {
    void onClickRecentItem(String id, String title, String subtitle, String country);
    void onClickClear(int index, String logId);
}
