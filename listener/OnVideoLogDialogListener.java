package com.song.solartube.listener;

/**
 * Created by Song on 2017-12-04.
 */

public interface OnVideoLogDialogListener {
    void onClickContinuePositive(String logId, long positions);
    void onClickContinueNegative(String logId);
    void onVideoLogExist(boolean exist);
}
