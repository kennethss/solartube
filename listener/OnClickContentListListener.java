package com.song.solartube.listener;

/**
 * Created by Song on 2018-01-04.
 */

public interface OnClickContentListListener {
    void onClickContentList(String id, int part);
}
