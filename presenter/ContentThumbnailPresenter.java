package com.song.solartube.presenter;

import com.song.solartube.callback.ContentListCallback;
import com.song.solartube.callback.RecentContentCallback;
import com.song.solartube.adapter.contract.ContentThumbnailAdapterContract;
import com.song.solartube.adapter.contract.ContentThumbnailContract;
import com.song.solartube.listener.OnClickThumbnailListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.ContentThumbnail;
import com.song.solartube.model.ContentThumbnailModel;
import com.song.solartube.singleton.APIManager;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Song on 2018-01-18.
 */

public class ContentThumbnailPresenter implements ContentThumbnailContract.Presenter,
        ContentThumbnailModel.ContentThumbnailCallback, OnClickThumbnailListener, ContentListCallback.Callback {

    private ContentThumbnailAdapterContract.Model adapterModel;
    private ContentThumbnailAdapterContract.View adapterView;
    private ContentThumbnailContract.View view;

    @Override
    public void attachView(ContentThumbnailContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void setContentThumbnailAdapterModel(ContentThumbnailAdapterContract.Model adapterModel) {
        this.adapterModel = adapterModel;
    }

    @Override
    public void setContentThumbnailAdapterView(ContentThumbnailAdapterContract.View adapterView) {
        this.adapterView = adapterView;
        this.adapterView.setOnClickListener(this);
    }

    @Override
    public void setRecentContentCallback(RecentContentCallback recentContentCallback) {
    }

    @Override
    public void loadMoreItems(int category, int size) {
        ContentThumbnailModel contentThumbnailModel = new ContentThumbnailModel(this);
        contentThumbnailModel.request(this, category, size);
    }

    @Override
    public void getContentThumbnail(List<ContentThumbnail> thumbnails) {
        adapterModel.addItems(thumbnails);
        adapterView.notifyAdapter();
    }

    @Override
    public void onClickThumbnail(final ContentThumbnail thumbnail) {
        Call<List<Content>> call = APIManager.getInstance().getApiCall().getContentDetailList(thumbnail.subtitle, thumbnail.country);
        view.showProgress("로딩중..", call);
        call.enqueue(new ContentListCallback(thumbnail.title, this));
    }

    @Override
    public void responseContentList(String title, List<Content> contents) {
        if(contents.size() != 1) {
            view.moveListPage(title, contents);
        } else {
            view.moveDetailPage(title, contents);
        }
    }
}
