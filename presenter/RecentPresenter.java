package com.song.solartube.presenter;

import android.content.Context;

import com.song.solartube.callback.RecentContentCallback;
import com.song.solartube.adapter.contract.RecentAdatperContract;
import com.song.solartube.adapter.contract.RecentContract;
import com.song.solartube.model.VideoLog;

import java.util.List;

/**
 * Created by Song on 2018-01-17.
 */

public class RecentPresenter implements RecentContract.Presenter{

    private RecentContract.View view;

    private RecentAdatperContract.Model adapterModel;
    private RecentAdatperContract.View adapterView;

    private RecentContentCallback callback;

    @Override
    public void attachView(RecentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {

    }

    @Override
    public void setRecentAdapterModel(RecentAdatperContract.Model adapterModel) {
        this.adapterModel = adapterModel;
    }

    @Override
    public void setRecentAdapterView(RecentAdatperContract.View adapterView) {
        this.adapterView = adapterView;
    }

    @Override
    public void setRecentContentCallback(RecentContentCallback recentContentCallback) {
        this.callback = new RecentContentCallback();
    }

    @Override
    public void loadItems(Context context, boolean isClear) {
        callback.request(new RecentContentCallback.RecentContentRequestCallback() {
            @Override
            public void getResult(List<VideoLog> logs) {
                adapterModel.addItems(logs);
                adapterView.notifyAdapter();
            }
        });
    }
}
