package com.song.solartube.callback;

import android.content.Context;
import android.content.DialogInterface;
import android.icu.text.Replaceable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.song.solartube.R;
import com.song.solartube.listener.OnVideoLogDialogListener;
import com.song.solartube.model.VideoLog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-04.
 */

public class VideoLogRequestCallback implements Callback<VideoLog>, MaterialDialog.SingleButtonCallback, DialogInterface.OnDismissListener {

    private final String TAG = "VideoLogRequestCallback";

    private Context mContext;
    private OnVideoLogDialogListener listener;

    private String logId;


    public VideoLogRequestCallback(Context context, OnVideoLogDialogListener listener) {
        mContext = context;
        this.listener = listener;
    }

    @Override
    public void onResponse(Call<VideoLog> call, final Response<VideoLog> response) {
        if (response.isSuccessful()) {
            logId = response.body().logId;
            listener.onVideoLogExist(true);

            new MaterialDialog.Builder(mContext)
                    .content(R.string.ask_exist_video_log)
                    .positiveColor(mContext.getResources().getColor(R.color.colorBlack))
                    .negativeColor(mContext.getResources().getColor(R.color.colorBlack))
                    .positiveText(R.string.agree)
                    .negativeText(R.string.cancel)
                    .tag(response.body().time)
                    .onPositive(this)
                    .onNegative(this)
                    .dismissListener(this)
                    .show();
        } else {
            listener.onVideoLogExist(false);
            Log.d(TAG, "비디오 로그 없음");
        }
    }

    @Override
    public void onFailure(Call<VideoLog> call, Throwable t) {
        listener.onVideoLogExist(false);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        switch (which){
            case POSITIVE:
                long position = (long)dialog.getTag();
                listener.onClickContinuePositive(logId, position);
                break;
            case NEGATIVE:
                listener.onClickContinueNegative(logId);
                break;
            case NEUTRAL:
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        listener.onClickContinueNegative(logId);
    }
}
