package com.song.solartube.callback;

import android.support.annotation.NonNull;

import com.song.solartube.model.VideoLog;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.UserManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2018-01-17.
 */

public class RecentContentCallback implements Callback<List<VideoLog>> {

    private RecentContentRequestCallback callback;

    public void request(RecentContentRequestCallback callback) {
        this.callback = callback;
        APIManager.getInstance().getApiCall().getRecentVideoContentLog(UserManager.getInstance().getUser().userId)
                .enqueue(this);
    }

    @Override
    public void onResponse(@NonNull Call<List<VideoLog>> call, @NonNull Response<List<VideoLog>> response) {
        if(response.isSuccessful()) {
            callback.getResult(response.body());
        }
    }

    @Override
    public void onFailure(Call<List<VideoLog>> call, Throwable t) {

    }

    public interface RecentContentRequestCallback{
        void getResult(List<VideoLog> logs);
    }
}
