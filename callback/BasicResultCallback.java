package com.song.solartube.callback;

import com.song.solartube.adapter.RecentContentAdapter;
import com.song.solartube.model.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-06.
 */

public class BasicResultCallback implements Callback<Result> {

    int index;
    RecentContentAdapter adapter = null;

    public BasicResultCallback (int index, RecentContentAdapter adapter) {
        this.adapter = adapter;
        this.index = index;
    }

    public BasicResultCallback () {

    }

    @Override
    public void onResponse(Call<Result> call, Response<Result> response) {
        if (adapter != null) {
            adapter.removeItem(this.index);
        }
    }

    @Override
    public void onFailure(Call<Result> call, Throwable t) {

    }
}
