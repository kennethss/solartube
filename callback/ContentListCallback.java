package com.song.solartube.callback;

import com.song.solartube.model.ContentThumbnailModel;
import com.song.solartube.singleton.MoveManager;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.model.Content;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-11-20.
 */

public class ContentListCallback implements Callback<List<Content>> {

    private BaseActivity baseActivity;
    private String searchTitle;
    private Callback callback;
    private String title;

    public ContentListCallback(BaseActivity baseActivity, String searchTitle) {
        this.baseActivity = baseActivity;
        this.searchTitle = searchTitle;
    }

    public ContentListCallback(String title, Callback callback) {
        this.title = title;
        this.callback = callback;
    }

    @Override
    public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
        if(response.isSuccessful()) {
            callback.responseContentList(this.title, response.body());
        }
    }

    @Override
    public void onFailure(Call<List<Content>> call, Throwable t) {
        call.clone().enqueue(this);
    }

    public interface Callback{
        void responseContentList(String title, List<Content> contents);
    }
}
