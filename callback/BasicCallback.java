package com.song.solartube.callback;

import com.song.solartube.adapter.RecentContentAdapter;
import com.song.solartube.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-05.
 */

public class BasicCallback implements Callback<User> {

    int index;
    RecentContentAdapter adapter;

    public BasicCallback(int index, RecentContentAdapter adapter) {
        this.adapter = adapter;
        this.index = index;
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        adapter.removeItem(this.index);
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
    }
}
