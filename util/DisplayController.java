package com.song.solartube.util;

import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Song on 2017-12-26.
 */

public class DisplayController {

    // 화면 꺼지지않게 유지
    public void keepTheScreenOn(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}
