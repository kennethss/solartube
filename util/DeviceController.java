package com.song.solartube.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Debug;
import android.util.Log;
import android.view.Display;

/**
 * Created by Song on 2017-12-08.
 */

public class DeviceController {


    public int getPortraitHeight(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);

        return size.y;
    }

    public int getPortraitWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);

        return size.x;
    }

    public void audioFocus(Context context) {
        VersionController versionController = new VersionController(context);
        int res = versionController.getAudioFocus(context);

        Log.d("AudioFocus" , toString().valueOf(res));

        if(res == AudioManager.AUDIOFOCUS_GAIN) {

        }
    }

    public Point getDisplaySize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size;
    }
}
