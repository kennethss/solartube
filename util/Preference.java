package com.song.solartube.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Song on 2017-12-07.
 */

public class Preference {
    private final String SOLAR_TUBE = "SolarTube";

    public void saveCurrentTime(Context context, String key, long value) {
        SharedPreferences s = context.getSharedPreferences(SOLAR_TUBE, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = s.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void saveCurrentString(Context context, String key, String value) {
        SharedPreferences s = context.getSharedPreferences(SOLAR_TUBE, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = s.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public long getLastTime(Context context, String key) {
        SharedPreferences s = context.getSharedPreferences(SOLAR_TUBE, context.MODE_PRIVATE);
        return s.getLong(key, 0);
    }

    public String getString(Context context, String key) {
        SharedPreferences s = context.getSharedPreferences(SOLAR_TUBE, context.MODE_PRIVATE);
        return s.getString(key, null);
    }
}
