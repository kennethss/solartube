package com.song.solartube.util;

import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.text.Spanned;

import com.song.solartube.BuildConfig;

import java.io.File;

/**
 * Created by Song on 2017-12-07.
 */

public class VersionController {
    private Context context;

    public VersionController(Context context) {
        this.context = context;
    }

    public void getFileAccess() {
        //Build.VERSION.SDK_INT >
        //
        // 7.0부터는 FileProvider를 사용해야함
    }

    /**
     * 앱 설치 인텐트 호출방식이 변경됨 기존 file:// -> content://
     * @param file
     */
    public void callAppInstallActivity(File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            intent.setData(apkUri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        } else {
            Uri apkUri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public static Spanned fromHtml(String html) {
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }

        return result;
    }

    /**
     * 사운드 Focus 관련 함수
     */
    public int getAudioFocus(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA) // 오디오를 사용할 앱의 타입
                    .setContentType(AudioAttributes.CONTENT_TYPE_MOVIE) // 오디오 콘텐츠 타입
                    .build();

            AudioFocusRequest focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setAudioAttributes(playbackAttributes)
                    .setAcceptsDelayedFocusGain(true)
                    .setOnAudioFocusChangeListener(null)
                    .build();

            return am.requestAudioFocus(focusRequest);
        } else {
            AudioManager.OnAudioFocusChangeListener listener = new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int i) {

                }
            };

            int focusResult = am.requestAudioFocus(listener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

            return focusResult;

            /*if(focusResult == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                // Do it
            }*/
        }
    }
}
