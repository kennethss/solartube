package com.song.solartube.util;

import android.text.Html;

/**
 * Created by Song on 2017-11-21.
 */

public class StringUtil {

    private static StringBuilder builder = new StringBuilder();

    public static String append(String... strings) {

        for (int i = 0; i < strings.length; i++) {
            builder.append(strings[i]);
        }

        String s = builder.toString();
        builder.setLength(0);
        return s;
    }
}
