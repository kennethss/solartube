package com.song.solartube.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Song on 2017-12-07.
 */

public class TimeController {

    public long getCurrentTime() {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String s = sdNow.format(date);

        return now;
    }

    public long endTime() {
        String time = "2017/12/08 19:02:12";
        SimpleDateFormat sdNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = null;
        try {
            date = sdNow.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assert date != null;
        return date.getTime();
    }
}
