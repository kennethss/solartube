package com.song.solartube;

import android.view.MenuItem;

import com.song.solartube.singleton.UserManager;
import com.song.solartube.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-11-20.
 */

public class ResultCallback implements Callback<User> {
    MenuItem menuItem;

    public ResultCallback(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        if (response.isSuccessful()) {
            User user = response.body();
            UserManager.getInstance().setUser(user);

            if (UserManager.getInstance().isPremiumAdult()) {
                menuItem.setVisible(true);
            }
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {

    }
}
