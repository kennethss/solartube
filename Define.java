package com.song.solartube;

/**
 * Created by Song on 2017-09-13.
 */

public class Define {
    public static final int HOME = 2;
    public static final int MOVIE_KOREA = 4;
    public static final int MOVIE_FOREIGN = 8;
    public static final int DRAMA_KOREA = 16;
    public static final int DRAMA_FOREIGN = 32;
    public static final int ANIMATION = 64;
    public static final int ENTERTAINMENT = 128;
    public static final int DOCUMENTARY = 256;
    public static final int ADULT = 512;

    public static int[] CATEGORY_CODE_ARRAY = new int[]{
            Define.HOME,
            Define.MOVIE_KOREA,
            Define.MOVIE_FOREIGN,
            Define.DRAMA_KOREA,
            Define.DRAMA_FOREIGN,
            Define.ENTERTAINMENT,
            Define.DOCUMENTARY,
            Define.ANIMATION,
            Define.ADULT };

    public static final String ADULT_KOREA = "AdultKorea";
    public static final String ADULT_KOREA_AV = "AdultKoreaAv";
    public static final String ADULT_JAPAN = "AdultJapan";
    public static final String ADULT_JAPAN_AV = "AdultJapanAV";
    public static final String ADULT_USA = "AdultUsa";
    public static final String ADULT_USA_AV = "AdultUsaAV";

    public static String[] CATEGORY_CODE_ADULT = new String[]{
            Define.ADULT_KOREA, Define.ADULT_KOREA_AV,
            Define.ADULT_JAPAN, Define.ADULT_JAPAN_AV,
            Define.ADULT_USA, Define.ADULT_USA_AV };

    public static final int SPECIAL_CODE = 10000;
    public static final String HOST = "http://wlsrhkd4023.iptime.org:10080/";
}
