package com.song.solartube.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.solar.sns.SocialManager;
import com.song.solartube.R;
import com.song.solartube.singleton.BusProvider;
import com.song.solartube.singleton.UserManager;
import com.song.solartube.SolarBusEvent;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.databinding.ActivitySettingBinding;
import com.squareup.otto.Subscribe;

/**
 * Created by Song on 2017-11-07.
 */

public class SettingActivity extends BaseActivity implements View.OnClickListener{
    ActivitySettingBinding binding;

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        setActionBar();

        BusProvider.getInstance().register(this);

        if(SocialManager.getInstance().isLogin(this)) {
            binding.settingLoginText.setText("로그아웃");
        } else {
            binding.settingLoginText.setText("로그인");
        }

        binding.settingLoginForm.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.setting_login_form:
                if(SocialManager.getInstance().isLogin(this)) {
                    SocialManager.getInstance().logout(this);
                    UserManager.getInstance().clearUserData();
                    binding.settingLoginText.setText("로그인");
                } else {
                    SocialManager.getInstance().openLoginActivty(this);
                }
                break;
        }
    }

    @Subscribe
    public void FinishLogin(SolarBusEvent event) {
        if(event.message == "login") {
            binding.settingLoginText.setText("로그아웃");
        }
    }
}
