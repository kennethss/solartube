package com.song.solartube.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.song.solartube.Define;
import com.song.solartube.EventLogger;
import com.song.solartube.R;
import com.song.solartube.TrackSelectionHelper;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.callback.BasicResultCallback;
import com.song.solartube.callback.VideoLogRequestCallback;
import com.song.solartube.databinding.ActivityContentDetailBinding;
import com.song.solartube.listener.CustomPlayerControllerListener;
import com.song.solartube.listener.OnVideoLogDialogListener;
import com.song.solartube.listener.SubtitleSettingListener;
import com.song.solartube.model.Content;
import com.song.solartube.model.ThumbnailModel;
import com.song.solartube.model.User;
import com.song.solartube.player.CustomModifyRequestPopupView;
import com.song.solartube.player.CustomPlayerController;
import com.song.solartube.player.CustomSubtitleSettingView;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.UserManager;
import com.song.solartube.util.DeviceController;
import com.song.solartube.util.Option;
import com.song.solartube.util.StringUtil;
import com.song.solartube.util.VersionController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-09-13.
 */

public class ContentDetailActivity extends BaseActivity implements Player.EventListener,
        SimpleExoPlayer.VideoListener, MaterialSpinner.OnItemSelectedListener, SubtitleSettingListener,
        CustomPlayerControllerListener, OnVideoLogDialogListener, View.OnClickListener, GestureDetector.OnGestureListener {

    ActivityContentDetailBinding binding;
    SimpleExoPlayer player;
    Uri uri;
    Uri subtitleUri;

    // Custom
    private CustomSubtitleSettingView subtitleSettingView;
    private CustomModifyRequestPopupView modifyRequestPopupView;
    private CustomPlayerController customPlayerController;

    // Content
    private String[] ids;
    private boolean isSubTitle;
    private boolean isVertical;
    private boolean isFullSize = false;
    private boolean isVideoLog;
    private boolean isMultiple;
    private String logId;
    private boolean isShowController = false;

    private Content content;
    private String contentTitle;
    private String thumbnail;

    // Player
    private EventLogger eventLogger;
    private DefaultTrackSelector trackSelector;
    private TrackSelectionHelper trackSelectionHelper;
    private SubtitleView subtitleView;

    private int resumeWindow;
    private long resumePosition;
    private boolean isControllerReady;
    private boolean isExistVideoLog;
    private boolean isLock;
    private AdView mAdView;

    private int currentVisibility;

    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private void updateResumePosition() {
        resumeWindow = player.getCurrentWindowIndex();
        resumePosition = Math.max(0, player.getContentPosition());
    }

    private void clearResumePosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }

    private int getOrientation() {
        return getResources().getConfiguration().orientation;
    }

    private boolean isLandscape(){
        int orientation = getResources().getConfiguration().orientation;

        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    private void rotateDisplay() {
        int orientation = getOrientation();

        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            if(isVertical) {
                    if(isFullSize){
                    int width = getResources().getDisplayMetrics().widthPixels;
                    FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, width * 9 / 16);
                    frameParams.gravity = Gravity.CENTER;
                    binding.videoView.setLayoutParams(frameParams);
                    binding.overlayFrame.setLayoutParams(frameParams);
                    binding.playerLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    isFullSize = false;
                    mAdView.setVisibility(View.VISIBLE);
                } else {

                    binding.videoView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    binding.overlayFrame.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    binding.playerLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    isFullSize = true;
                    mAdView.setVisibility(View.GONE);
                }

            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }

        }
    }

    private void releasePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            updateResumePosition();
            player.release();
            player = null;
            trackSelector = null;
            trackSelectionHelper = null;
            eventLogger = null;
        }
    }

    private void setSubtitleView(SubtitleView s) {
        CaptionStyleCompat captionStyleCompat = new CaptionStyleCompat(
                Color.WHITE,
                Color.TRANSPARENT,
                Color.TRANSPARENT,
                CaptionStyleCompat.EDGE_TYPE_DROP_SHADOW,
                Color.BLACK,
                Typeface.createFromAsset(getAssets(), "fonts/NanumBarunGothic.ttf"));

        s.setStyle(captionStyleCompat);
        s.setFadingEdgeLength(2);
        s.setApplyEmbeddedStyles(true);
        s.setApplyEmbeddedFontSizes(true);
        s.setUserDefaultTextSize();
        //s.setFixedTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
    }

    private void setSurfaceView() {
        SimpleExoPlayerView previewPlayerView = findViewById(R.id.previewPlayerView);
        View view = previewPlayerView.getVideoSurfaceView();

        if (view instanceof SurfaceView) {
            SurfaceView surfaceView = (SurfaceView) view;
            surfaceView.setZOrderMediaOverlay(true);
            surfaceView.setZOrderOnTop(true);
            surfaceView.setVisibility(View.INVISIBLE);
        }
    }

    private void initSubtitleSettingView() {
        DeviceController d = new DeviceController();
        subtitleSettingView = new CustomSubtitleSettingView(getLayoutInflater(), d.getPortraitHeight(this));
        subtitleSettingView.setOnSubtitleListener(this);
        subtitleView = binding.videoView.getSubtitleView();
    }

    private void initModifyRequestPopupView() {
        DeviceController d = new DeviceController();
        modifyRequestPopupView = new CustomModifyRequestPopupView(getLayoutInflater(), d.getPortraitHeight(this));
    }

    private void initializeData() {
        Bundle bundle = getIntent().getExtras();

        String[] ids = new String[0];
        String[] titles = new String[0];
        int[] parts = new int[0];
        boolean isMulti = false;

        if(bundle != null) {
            ids = bundle.getStringArray("ids");
            titles = bundle.getStringArray("titles");
            parts = bundle.getIntArray("parts");
            isMulti = bundle.getBoolean("multiple");
        }

        this.ids = ids;
        this.content = ThumbnailModel.getInstance().getContentDetail(ids[0]);

        this.thumbnail = Define.HOST + content.path + "/" + content.thumbnail;

        if(isMulti) {
            this.contentTitle = content.title + " - " + content.part + "화";
        } else {
            this.contentTitle = content.title;
        }

        // 동영상 ,자막 URL 설정
        String url = Define.HOST + content.path + "/" + content.contentId;

        uri = Uri.parse(url);
        subtitleUri = Uri.parse(url + ".srt");

        if(content.isSubTitle != 0) {
            isSubTitle = true;
        }

        if(ids.length != 1) {
            ArrayList<String> partsText = new ArrayList<>();

            for (int i=0; i<ids.length; i++) {
                if(parts[i] < Define.SPECIAL_CODE) {
                    partsText.add(parts[i] + "화  " + titles[i]);
                } else {
                    int part = parts[i] - Define.SPECIAL_CODE;
                    partsText.add("스페셜 " + (part+1) + "화");
                }
            }

            setSpinnerDropdown(partsText);
            isMultiple = true;
        } else {
            isMultiple = false;
            binding.spinnerDropdown.setVisibility(View.GONE);
        }
    }

    private void initCustomController() {
        customPlayerController = new CustomPlayerController(this);
        customPlayerController.setOnCustomPlayerControllerListener(this);
        customPlayerController.setContentTitle(contentTitle);

        // todo 프리뷰 처리
        //previewTimeBarLayout = findViewById(R.id.previewSeekBarLayout);
        //previewTimeBar = findViewById(R.id.exo_progress);
    }

    private void initializePlayer() {
        boolean needNewPlayer = player == null;

        if(needNewPlayer) {

        }

        TrackSelection.Factory adaptiveTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);

        trackSelector = new DefaultTrackSelector(adaptiveTrackSelectionFactory);
        trackSelectionHelper = new TrackSelectionHelper(trackSelector, adaptiveTrackSelectionFactory);
        eventLogger = new EventLogger(trackSelector);

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "SolarTube"), BANDWIDTH_METER);

        MediaSource videoSource = getVideoMediaSource(this.uri, dataSourceFactory);
        MediaSource mediaSourceWithSubtitle;

        if(isSubTitle) {
            MediaSource textMediaSource = getSubTitleMediaSource(this.subtitleUri, dataSourceFactory);
            mediaSourceWithSubtitle = new MergingMediaSource(videoSource, textMediaSource);
        } else {
            mediaSourceWithSubtitle = videoSource;
        }

        // Player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        player.addListener(this);
        player.addListener(eventLogger);
        player.addMetadataOutput(eventLogger);
        player.setAudioDebugListener(eventLogger);
        player.setVideoDebugListener(eventLogger);
        player.setPlayWhenReady(false);
        player.getCurrentTimeline();
        player.addVideoListener(this);

        // PlayerView
        binding.videoView.setPlayer(player);
        binding.videoView.setOnTouchListener(customPlayerController);
        // PlayerView Controller
        binding.videoView.setControllerAutoShow(false);       // 컨트롤러 자동으로 보이게
        binding.videoView.setControllerShowTimeoutMs(0);      // 컨트롤러 타임아웃
        binding.videoView.setUseController(true);             // 컨트롤러 사용 여부
        binding.videoView.setControllerHideOnTouch(false);    // 컨트롤러 터치로 사라지게
        binding.videoView.setControllerVisibilityListener(customPlayerController);

        binding.playerLayout.setOnClickListener(this);

        // SubtitleView
        setSubtitleView(binding.videoView.getSubtitleView());

        //PlaybackControlView.ControlDispatcher controlDispatcher = new PlaybackControlView.ControlDispatcher();
        //controlDispatcher.dispatchSetPlayWhenReady(player, true);
        //playbackControlView.setControlDispatcher(controlDispatcher);

        // 영상 정보찍는거
        // debugTextViewHelper = new DebugTextViewHelper(player,)
        // debugTextViewHelper.start();

        boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            player.seekTo(resumeWindow, resumePosition);
        }

        player.prepare(mediaSourceWithSubtitle, !haveResumePosition, false);
    }

    private void setContentDetailData(String title, Content content) {
        binding.contentTitle.setText(title);
        binding.contentDate.setText(content.contentDate);
        binding.contentGenre.setText(content.genre);
        binding.contentLength.setText(content.videoLength + "분");
        binding.contentRating.setText(content.rating + "세 이상 관람가");
        binding.contentSize.setText(content.size);

        binding.contentActor.setText(content.actor);
        binding.contentDirector.setText(content.director);
        binding.contentDescription.setText(VersionController.fromHtml(content.description));
    }

    private void setSpinnerDropdown(ArrayList<String> parts) {
        binding.spinnerDropdown.setItems(parts);
        binding.spinnerDropdown.setDropdownMaxHeight(450);
        binding.spinnerDropdown.setBackground(getResources().getDrawable(R.drawable.square));
        binding.spinnerDropdown.setOnItemSelectedListener(this);
    }

    private MediaSource getSubTitleMediaSource(Uri uri, DataSource.Factory dataSourceFactory) {
        Format subtitleFormat = Format.createTextSampleFormat(
                Util.getUserAgent(this, "SolarTube"), // An identifier for the track. May be null.
                MimeTypes.APPLICATION_SUBRIP, // The mime type. Must be set correctly.
                C.SELECTION_FLAG_DEFAULT, // Selection flags for the track.
                "ko-KR"); // The subtitle language. May be null.

        MediaSource textMediaSource = new SingleSampleMediaSource(
                uri,
                //Uri.parse("http://180.71.238.81:10080/SolarTube/Video/Movie/USA/A3.smi"),
                dataSourceFactory,
                subtitleFormat,
                C.TIME_UNSET,0 );

        return textMediaSource;
    }

    private MediaSource getVideoMediaSource(Uri uri, DataSource.Factory dataSourceFactory) {
        MediaSource videoSource = new ExtractorMediaSource(
                uri,
                dataSourceFactory,  // DataSource is AppName or BandWidthMeter
                new DefaultExtractorsFactory(),  // 추출하기 위한 팩토리
                null,
                null);

        return videoSource;
    }

    private void requestVideoLog() {
        User user = UserManager.getInstance().getUser();

        if (user != null) {
            APIManager.getInstance().getApiCall().getVideoLog(content.contentId, user.userId)
                    .enqueue(new VideoLogRequestCallback(this, this));
        }
    }

    private void addVideoLog() {
        if(UserManager.getInstance().getUser() != null) {
            if(!isVideoLog) {
                String title;

                if (isMultiple) {
                    if(this.contentTitle.equals(content.title)) {
                        title = StringUtil.append(this.contentTitle, " ", String.valueOf(content.part), "화");  // Ex) 태양의 후예 1화
                    } else {
                        title = StringUtil.append(this.contentTitle, " ", String.valueOf(content.part), "화", " - ", content.title);  // Ex) 진격의 거인 2화 - 시간지나
                    }

                } else {
                    title = content.title; // 끝까지 간다
                }

                if (player.getCurrentPosition() != 0) {
                    APIManager.getInstance().getApiCall().addVideoLog(content.contentId, thumbnail, this.contentTitle,
                            content.subtitle, content.country, UserManager.getInstance().getUser().userId,
                            (int) player.getCurrentPosition(), (int) player.getDuration()).enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                        }
                    });
                }
            }else {
                SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String saveTime = sdfNow.format(new Date(System.currentTimeMillis()));

                APIManager.getInstance().getApiCall().updateVideoLog(logId, (int)player.getCurrentPosition(), saveTime)
                        .enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {

                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                            }
                        });
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_content_detail);
        binding.setContentActivity(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        clearResumePosition();

        // 초기화면의 엑소플레이어를 16 : 9 의 비율로 맞춘다(Custom 레이아웃은 문제가있음)
        int width = getResources().getDisplayMetrics().widthPixels;
        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, width * 9 / 16);
        frameParams.gravity = Gravity.CENTER;

        binding.videoView.setLayoutParams(frameParams);
        binding.overlayFrame.setLayoutParams(frameParams);
        binding.playerLayout.setOnClickListener(this);
        //binding.playerLayout.setOnTouchListener(this);


        initializeData();
        setContentDetailData(contentTitle, content);

        initCustomController();

        initSubtitleSettingView();
        initModifyRequestPopupView();
        setSurfaceView();
        requestVideoLog();

        mAdView = findViewById(R.id.adView_detail);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        releasePlayer();
        clearResumePosition();
        setIntent(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        if(isLandscape()){
            getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);
        }

        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            addVideoLog();
            releasePlayer();
        }
    }

    @Override
    public void onBackPressed() {
        if(isLandscape()) {
            rotateDisplay();
        } else {
            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            addVideoLog();
            releasePlayer();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //int flags = WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        int flags = WindowManager.LayoutParams.FLAG_FULLSCREEN;// | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        int masks = WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;

        switch (newConfig.orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                customPlayerController.setVisibleContentTitle(View.VISIBLE);
                customPlayerController.setVisibleMoreVertical(View.VISIBLE);

                //binding.arrowBack.setVisibility(View.GONE);
                mAdView.setVisibility(View.GONE);
                binding.videoView.setTouchscreenBlocksFocus(false);

                customPlayerController.setStatusIsLandscape(this, true);

                getWindow().addFlags(flags);
                getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);

                binding.videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                binding.playerLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                binding.videoView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                binding.overlayFrame.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                break;

            case Configuration.ORIENTATION_PORTRAIT:
                customPlayerController.setVisibleContentTitle(View.GONE);
                customPlayerController.setVisibleMoreVertical(View.GONE);
                //binding.arrowBack.setVisibility(View.VISIBLE);
                mAdView.setVisibility(View.VISIBLE);

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                getWindow().getDecorView().setSystemUiVisibility(0);

                customPlayerController.setStatusIsLandscape(this, false);

                binding.videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearParams.gravity = Gravity.CENTER;
                binding.playerLayout.setLayoutParams(linearParams);

                FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, binding.videoView.getHeight() * 9 / 16);
                frameParams.gravity = Gravity.CENTER;
                binding.videoView.setLayoutParams(frameParams);
                binding.overlayFrame.setLayoutParams(frameParams);

                break;
        }
    }

    // % 화를 선택하였을때 콜백 리스너 (MaterialSpinner.OnItemSelectedListener)
    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        releasePlayer();
        clearResumePosition();
        setContentDetailData(null, ThumbnailModel.getInstance().getContentDetail(ids[position]));
        initializePlayer();
        player.seekTo(0);
    }

    // 자막 이벤트 리스너 (SubtitleSettingListener)
    @Override
    public void onChangedTextSize(float size) {
        subtitleView.setFixedTextSize(TypedValue.COMPLEX_UNIT_DIP, size);
    }

    @Override
    public void onChangedBottomPadding(float size) {
        subtitleView.setBottomPaddingFraction(size / 100);
    }

    // 플레이어 이벤트 (Player.EventListener)
    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case Player.STATE_READY:
                if(playWhenReady) {
                    DeviceController d = new DeviceController();
                    d.audioFocus(ContentDetailActivity.this);
                }

                customPlayerController.getPlayFrame().setVisibility(View.VISIBLE);
                binding.playerDialog.setVisibility(View.GONE);
                break;

            case Player.STATE_BUFFERING:
                binding.playerDialog.setVisibility(View.VISIBLE);
                customPlayerController.getPlayFrame().setVisibility(View.GONE);
                break;

            case Player.STATE_ENDED:
                if (this.logId != null) {
                    APIManager.getInstance().getApiCall().deleteVideoLog(this.logId).enqueue(new BasicResultCallback());
                }

                player.seekTo(0);
                player.setPlayWhenReady(false);
                break;

            case Player.STATE_IDLE:
                binding.playerDialog.setVisibility(View.GONE);
                binding.videoView.setLayoutParams(new ViewGroup.LayoutParams(binding.videoView.getWidth(), binding.videoView.getWidth() / 9));
                binding.overlayFrame.setLayoutParams(new ViewGroup.LayoutParams(binding.videoView.getWidth(), binding.videoView.getWidth() / 9));
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity() {
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }


    // 비디오 사이즈 변경 리스너 SimpleExoPlayer.VideoListener
    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        isVertical = width <= height;
    }

    @Override
    public void onRenderedFirstFrame() {

    }

    // CustomPlayerControllerListener
    @Override
    public void onClickLandScape() {
        rotateDisplay();
    }

    @Override
    public void onClickControllerMore() {
        customPlayerController.showBottomSheet();
    }

    @Override
    public void onClickArrowBack() {
        if(getOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
            rotateDisplay();
        } else {
            finish();
        }
    }

    @Override
    public void onClickResizeFit() {
        binding.videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);
        customPlayerController.hideBottomSheet();
    }

    @Override
    public void onClickResizeFill() {
        binding.videoView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
        getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);
        customPlayerController.hideBottomSheet();
    }

    @Override
    public void onClickSubtitleOption() {
        if(subtitleSettingView.isShowing()) {
            subtitleSettingView.hidePopupView();
        } else {
            subtitleSettingView.showPopupView();
        }

        customPlayerController.hideBottomSheet();
    }

    @Override
    public void onClickModifyRequest() {
        if(modifyRequestPopupView.isShowing()) {
            modifyRequestPopupView.hidePopupView();
        } else {
            modifyRequestPopupView.showPopupView(content.contentId, player.getCurrentPosition());
        }

        customPlayerController.hideBottomSheet();
    }

    @Override
    public void onClickController() {
        Toast.makeText(this, "컨트롤러! = " + binding.videoView.getUseController(), Toast.LENGTH_SHORT).show();

        /*if(binding.videoView.getUseController()) {
            binding.videoView.controller
        }*/
        //binding.videoView.hideController();
        //binding.videoView.setUseController(false);

        // 컨트롤러 터치
        //Toast.makeText(this, "onClickController", Toast.LENGTH_SHORT).show();

        /*if(!isShowController) {
            binding.videoView.setUseController(true);
            binding.videoView.showController();
        } else {
            binding.videoView.setUseController(false);
            binding.videoView.hideController();
        }

        isShowController = !isShowController;*/

        /*if(binding.videoView.getControllerHideOnTouch()) {
            binding.videoView.hideController();
        } else {
            binding.videoView.showController();
        }*/

        //binding.videoView.setUseController(!binding.videoView.getUseController());
    }

    @Override
    public void onDragStart(boolean play) {
        if(play) {
            player.setPlayWhenReady(player.getPlayWhenReady());
        } else {
            isControllerReady = player.getPlayWhenReady();
            player.setPlayWhenReady(false);
            //binding.videoView.hideController();
        }
    }

    @Override
    public void onDragEnd(long position) {
        player.seekTo(player.getCurrentPosition() + position);
        player.setPlayWhenReady(isControllerReady);
    }

    @Override
    public void onShowBottomSheet() {
        getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);
    }

    @Override
    public void onHideBottomSheet() {
        int flags = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().addFlags(flags);
        getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);
    }

    @Override
    public void onHideUiOptions() {
        getWindow().getDecorView().setSystemUiVisibility(Option.HIDE_UI_OPTIONS);
    }

    @Override
    public void onClickContinuePositive(String logId, long positions) {
        this.logId = logId;
        player.seekTo(positions);
    }

    @Override
    public void onClickContinueNegative(String logId) {
        this.logId = logId;
    }

    @Override
    public void onVideoLogExist(boolean exist) {
        isVideoLog = exist;
    }

    // View.OnClickListener
    @Override
    public void onClick(View view) {
        Toast.makeText(this, "Relative View 온클릭", Toast.LENGTH_SHORT).show();

        switch (view.getId()){
            case R.id.player_layout:
                //binding.videoView.setUseController(true);
                //binding.videoView.showController();
                /*Toast.makeText(this, "PlayerLayout 클릭! = " + String.valueOf(isShowController), Toast.LENGTH_SHORT).show();
                if(!isShowController) {
                    binding.videoView.setUseController(true);
                    binding.videoView.showController();
                } else {
                    binding.videoView.setUseController(false);
                    binding.videoView.hideController();
                }

                isShowController = !isShowController;*/

                break;
        }
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        customPlayerController.onSingleTapUp(motionEvent);
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        customPlayerController.onScroll(motionEvent, motionEvent1, v, v1);
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }
}
