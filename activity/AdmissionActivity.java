package com.song.solartube.activity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.song.solartube.Define;
import com.song.solartube.R;
import com.song.solartube.customlayout.AdmissionVideoLayout;
import com.song.solartube.model.Result;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.DialogManager;
import com.song.solartube.singleton.MoveManager;
import com.song.solartube.util.Preference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-19.
 */

public class AdmissionActivity extends AppCompatActivity implements View.OnClickListener,
        SingleButtonCallback, Callback<Result> {

    private String code;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preference p = new Preference();
        String code = p.getString(this, "code");

        if(code != null) {
            MoveManager.getInstance().startActivity(this, MainActivity.class);
            finish();
        } else {
            setContentView(R.layout.activity_admission);

            AppCompatButton admissionButton = findViewById(R.id.admission_button);
            admissionButton.setOnClickListener(this);

            AdmissionVideoLayout videoView = findViewById(R.id.video);
            videoView.setVideoURI(Uri.parse(Define.HOST + "SolarTube/Resources/Video/AdmissionVideo.mp4"));
            videoView.start();
        }
    }

    @Override
    public void onClick(View view) {
        new MaterialDialog.Builder(this)
                .title("코드를 입력하세요")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .widgetColor(getResources().getColor(R.color.colorRed))
                .inputRangeRes(0, 10, R.color.colorRed)
                .input("ex)33819237", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        code = input.toString();
                    }
                })
                .alwaysCallInputCallback()
                .onPositive(this)
                .positiveColor(getResources().getColor(R.color.colorBlack))
                .show();
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        APIManager.getInstance().getApiCall().registerAdmissionCode(this.code).enqueue(this);
    }

    @Override
    public void onResponse(Call<Result> call, Response<Result> response) {
        if (response.code() == 200) {
            if (response.isSuccessful()) {
                if (response.body().resultCode == 200) {
                    Preference p = new Preference();
                    p.saveCurrentString(this, "code", this.code);
                    MoveManager.getInstance().startActivity(this, MainActivity.class);
                    finish();
                } else if(response.body().resultCode == 400) {
                    DialogManager.getInstance().showDefaultDialog(this, response.body().message);
                }
            }
        } else if (response.code() == 204) {
            DialogManager.getInstance().showDefaultDialog(this, response.message());
        } else {
            Toast.makeText(this, response.message(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<Result> call, Throwable t) {
        Log.d("에러", t.toString());
        Toast.makeText(this, "요청에 실패했습니다. 다시 시도해 주세요" + t.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
