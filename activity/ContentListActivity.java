package com.song.solartube.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.song.solartube.R;
import com.song.solartube.adapter.ContentListAdapter;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.singleton.MoveManager;

/**
 * Created by Song on 2018-01-04.
 */

public class ContentListActivity extends BaseActivity {

    private void setActionBar(String title) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    private void setAdView() {
        AdView adview = findViewById(R.id.adView_detail);
        AdRequest adRequest = new AdRequest.Builder().build();
        adview.loadAd(adRequest);
    }

    private void setRecyclerView(Bundle bundle) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        ContentListAdapter adapter = new ContentListAdapter(this, bundle);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(null, 1,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_adview);

        Bundle bundle = getIntent().getExtras();

        String title = null;
        boolean multiple = false;

        if (bundle != null) {
            title = bundle.getString("title");
            multiple = bundle.getBoolean("multiple");
        }

        setActionBar(title);
        setAdView();
        setRecyclerView(bundle);

        if(multiple) {
            //String[] ids = bundle.getStringArray("ids");
            MoveManager.getInstance().moveSingleContentPage(this, bundle.getString("id"), 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
