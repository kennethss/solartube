package com.song.solartube.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.jaredrummler.materialspinner.MaterialSpinner.OnItemSelectedListener;
import com.song.solartube.R;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.databinding.ActivityRequestContentBinding;
import com.song.solartube.model.Result;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.UserManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-06.
 */

public class RequestContentActivity extends BaseActivity implements OnItemSelectedListener, Callback<Result>,
        View.OnClickListener{

    private String selectCategory;
    private ActivityRequestContentBinding binding;

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setSpinner(String[] array) {
        binding.spinnerDropdown.setItems(array);
        binding.spinnerDropdown.setDropdownMaxHeight(450);
        binding.spinnerDropdown.setBackground(getResources().getDrawable(R.drawable.square));
        binding.spinnerDropdown.setOnItemSelectedListener(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_request_content);

        String[] array = getResources().getStringArray(R.array.category);
        array[0] = "없음";
        selectCategory = "없음";

        setSpinner(array);
        setActionBar();

        //inputText = findViewById(R.id.)
        binding.requestSendButton.setOnClickListener(this);

        AdView mAdView = findViewById(R.id.adView_request);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        selectCategory = item.toString();
    }

    @Override
    public void onClick(View view) {
        progressOn();
        int id = UserManager.getInstance().getUser().userId;
        APIManager.getInstance().getApiCall().addRequestContent(
                id, selectCategory,  binding.requestEditText.getText().toString()).enqueue(this);
    }

    @Override
    public void onResponse(Call<Result> call, Response<Result> response) {
        if (response.isSuccessful()) {
            progressOFF();

            if (response.body().resultCode == 0) {
                Toast.makeText(this, "성공적으로 요청했습니다!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFailure(Call<Result> call, Throwable t) {

    }
}
