package com.song.solartube.activity;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.solar.sns.OnUserStatusListener;
import com.solar.sns.SocialManager;
import com.solar.sns.UserData;
import com.solar.sns.custom.CustomProfileLayout;
import com.song.solartube.Define;
import com.song.solartube.R;
import com.song.solartube.ResultCallback;
import com.song.solartube.SearchDataListCallback;
import com.song.solartube.SolarBusEvent;
import com.song.solartube.VersionChecker;
import com.song.solartube.adapter.FragmentAdapter;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.base.BaseFragmentStatePagerAdapter;
import com.song.solartube.callback.ContentListCallback;
import com.song.solartube.fragment.AdultFragment;
import com.song.solartube.fragment.MainFragment;
import com.song.solartube.model.Content;
import com.song.solartube.model.SearchData;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.BusProvider;
import com.song.solartube.singleton.MoveManager;
import com.song.solartube.singleton.SolarTubeManger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class MainActivity extends BaseActivity implements SearchView.OnQueryTextListener,
        NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener, View.OnClickListener, OnUserStatusListener,
        MaterialSearchView.OnQueryTextListener{

    private ActivityMainBinding binding;

    private long pressedTime;
    private CustomProfileLayout customProfileLayout;
    private LinearLayout headerLoginLayout;
    private MenuItem adultItem;
    private MenuItem contentItem;
    private DrawerLayout drawer;
    private HashMap<String, SearchData> searchDataHashMap = new HashMap<>();
    private MaterialSearchView materialSearchView;

    private void initSearchView() {
        materialSearchView = findViewById(R.id.search_view);
        materialSearchView.setSubmitOnClick(true);
        materialSearchView.setEllipsize(true);
        materialSearchView.setOnQueryTextListener(this);
        materialSearchView.onFilterComplete(5);
        //materialSearchView.setOnSearchViewListener(this);
    }

    private void requestSearchList() {
        APIManager.getInstance().getApiCall().getContentAllSearchList()
                .enqueue(new SearchDataListCallback(materialSearchView, searchDataHashMap));
    }

    private void setTabLayout(){
        ViewPager viewPager = findViewById(R.id.viewpager);

        if (viewPager != null) {
            viewPager = setupViewPager(viewPager);
        }

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(Color.WHITE, Color.WHITE);
    }

    private void setNavigation(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        Menu navMenu = navigationView.getMenu();
        adultItem = navMenu.findItem(R.id.nav_19);
        contentItem = navMenu.findItem(R.id.nav_content);

        // Navigation Header View
        View headerView = navigationView.getHeaderView(0);
        headerLoginLayout = headerView.findViewById(R.id.nav_to_login);
        headerLoginLayout.setOnClickListener(this);

        customProfileLayout = headerView.findViewById(R.id.nav_header_profile);
        headerLoginLayout = headerView.findViewById(R.id.nav_to_login);

        SocialManager.getInstance().setCustomProfileLayout(customProfileLayout);
        SocialManager.getInstance().addUserStatusListener(this);

        String type = SocialManager.getInstance().getSharedPreference(this);

        // 로그인 상태에 따른 헤더뷰
        if (SocialManager.getInstance().isLogin(this)) {
            headerLoginLayout.setVisibility(View.GONE);
            customProfileLayout.setVisibility(View.VISIBLE);
            SocialManager.getInstance().requestSocialUser(this, customProfileLayout);
        } else {
            if (type != null && !type.isEmpty()) {
                UserData userData = SocialManager.getInstance().getUserData(type);
            }

            headerLoginLayout.setVisibility(View.VISIBLE);
            customProfileLayout.setVisibility(View.GONE);
        }
    }

    private void setAdView() {
        AdRequest adRequest = new AdRequest.Builder().build();
        AdView adView = findViewById(R.id.adView_main);
        adView.loadAd(adRequest);
    }

    private ViewPager setupViewPager(ViewPager viewPager) {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());

        String[] category = getResources().getStringArray(R.array.category);

        for (int i=0; i<category.length; i++) {
            MainFragment fragment = new MainFragment();
            fragment.initAdapter(Define.CATEGORY_CODE_ARRAY[i]);

            adapter.addFragment(fragment, category[i]);
        }

        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(adapter);

        return viewPager;
    }

    private void setEnableMenu(MenuItem menu, boolean enable) {
        if (enable) {
            menu.setEnabled(true);
            menu.getIcon().setAlpha(255);
        } else {
            menu.setEnabled(false);
            menu.getIcon().setAlpha(120);
        }
    }

    private void checkUpdate() {
        VersionChecker v = new VersionChecker(this);
        v.versionCheck();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setTheme(R.style.AppTheme);
        BusProvider.getInstance().register(this); // SettingActivity 로그아웃, 로그인 변경을 위한 이벤트

        setTabLayout();
        setAdView();
        setNavigation();

        initSearchView();
        requestSearchList();

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        checkUpdate();
    }

    @Override
    protected void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        materialSearchView.setMenuItem(search);
        return true;
    }

    private void closeCheck() {
        if ( pressedTime == 0 ) {
            Toast.makeText(MainActivity.this, " 한 번 더 누르면 종료됩니다." , Toast.LENGTH_LONG).show();
            pressedTime = System.currentTimeMillis();
        }
        else {
            int seconds = (int) (System.currentTimeMillis() - pressedTime);

            if ( seconds > 2000 ) {
                Toast.makeText(MainActivity.this, " 한 번 더 누르면 종료됩니다." , Toast.LENGTH_LONG).show();
                pressedTime = 0 ;
            }
            else {
                super.onBackPressed();
                //finish(); // app 종료 시키기
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START) || materialSearchView.isSearchOpen()) {
            if(drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawers();
            }

            if(materialSearchView.isSearchOpen()){
                materialSearchView.closeSearch();
            }
        } else {
            closeCheck();
        }
    }

    // View OnClick
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.nav_to_login:
                SocialManager.getInstance().openLoginActivty(view.getContext());
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.recent_content:
                SolarTubeManger.getInstance().moveToLoginCheckAfterActivity(this, RecentContentActivity.class);
                break;

            case R.id.nav_request:
                SolarTubeManger.getInstance().moveToLoginCheckAfterActivity(this, RequestContentActivity.class);
                break;

            case R.id.nav_setting:
                MoveManager.getInstance().startActivity(this, SettingActivity.class);
                break;

            case R.id.nav_content:
                ColorDrawable colorDrawable1 = new ColorDrawable(getResources().getColor(R.color.colorBlack));
                getSupportActionBar().setBackgroundDrawable(colorDrawable1);
                setTabLayout();

                setEnableMenu(adultItem, true);
                setEnableMenu(contentItem, false);
                break;

            case R.id.nav_19:
                ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.colorAccent));
                getSupportActionBar().setBackgroundDrawable(colorDrawable);

                ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

                String[] category = getResources().getStringArray(R.array.adult);
                ArrayList<Fragment> array = new ArrayList<>();

                for (int i=0; i<category.length; i++) {
                    AdultFragment fragment = new AdultFragment();
                    fragment.initAdapter(Define.CATEGORY_CODE_ADULT[i]);
                    array.add(fragment);
                }

                BaseFragmentStatePagerAdapter baseAdapter = new BaseFragmentStatePagerAdapter(
                        getSupportFragmentManager(), 3, array, category) {
                };

                viewPager.removeOnPageChangeListener(this); //addOnPageChangeListener(this);
                viewPager.setAdapter(baseAdapter);
                viewPager.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));

                contentItem.setVisible(true);
                setEnableMenu(adultItem, false);
                setEnableMenu(contentItem, true);

                break;
        }

        drawer.closeDrawers();

        return true;
    }

    // Social Login & Logout
    @Override
    public void loginFinished(boolean b) {
        if(b) {
            SocialManager.getInstance().requestSocialUser(this, customProfileLayout);
            headerLoginLayout.setVisibility(View.GONE);
            customProfileLayout.setVisibility(View.VISIBLE);

            BusProvider.getInstance().post(new SolarBusEvent("login"));
        }
    }

    @Override
    public void logoutFinished(boolean b) {
        if(b) {
            headerLoginLayout.setVisibility(View.VISIBLE);
            customProfileLayout.setVisibility(View.GONE);
            contentItem.setVisible(true);
            setEnableMenu(contentItem, true);
            adultItem.setVisible(false);
        }
    }

    @Override
    public void requestUserData(final String type, final String id, final String name) {
        if(id != null) {
            APIManager.getInstance().getApiCall().registerSolarTube(id, name, type)
                    .enqueue(new ResultCallback(adultItem));
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("포지션", "포지션 = " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    // Search
    @Override
    public boolean onQueryTextSubmit(String query) {
        if(searchDataHashMap.containsKey(query)) {
            SearchData searchData = searchDataHashMap.get(query);

            Call<List<Content>> call = APIManager.getInstance().getApiCall().getContentDetailList(searchData.subtitle, searchData.country);
            progressOn("Loading...", call);
            call.enqueue(new ContentListCallback(searchData.title, new ContentListCallback.Callback() {
                @Override
                public void responseContentList(String title, List<Content> contents) {
                    if(contents.size() != 1) {
                        MoveManager.getInstance().moveContentListPage(MainActivity.this, title, contents);
                    } else {
                        MoveManager.getInstance().moveContentDetailPage(MainActivity.this, title, contents);
                    }
                }
            }));
        }

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
