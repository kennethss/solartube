package com.song.solartube.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.song.solartube.R;
import com.song.solartube.callback.RecentContentCallback;
import com.song.solartube.adapter.contract.RecentContract;
import com.song.solartube.presenter.RecentPresenter;
import com.song.solartube.adapter.RecentContentAdapter;
import com.song.solartube.base.BaseActivity;
import com.song.solartube.model.VideoLog;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-11-21.
 */

public class RecentContentActivity extends BaseActivity implements RecentContract.View, Callback<List<VideoLog>> {

    RecyclerView recyclerView;
    TextView emptyTextView;

    RecentContentAdapter adapter;
    RecentContentCallback callback;

    RecentPresenter recentPresenter;

    private void setActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setPresenter() {
        callback = new RecentContentCallback();

        recentPresenter = new RecentPresenter();
        recentPresenter.attachView(this);
        recentPresenter.setRecentAdapterModel(adapter);
        recentPresenter.setRecentAdapterView(adapter);
        recentPresenter.setRecentContentCallback(callback);
        recentPresenter.loadItems(this, false);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_content);

        setActionBar();
        recyclerView = findViewById(R.id.recycler_view);
        emptyTextView = findViewById(R.id.empty_message);

        adapter = new RecentContentAdapter(this);

        ViewGroup.MarginLayoutParams marginLayoutParams =
                (ViewGroup.MarginLayoutParams) recyclerView.getLayoutParams();

        marginLayoutParams.setMargins(30, 0, 30, 0);

        GridLayoutManager manager = new GridLayoutManager(null, 1,
                LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutParams(marginLayoutParams);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemDecoration decoration = new DividerItemDecoration(this, LinearLayout.VERTICAL);

        setPresenter();

        AdView mAdView = findViewById(R.id.adView_recent);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<List<VideoLog>> call, Response<List<VideoLog>> response) {
        if (response.isSuccessful()) {

            if(response.body().size() != 0) {
                adapter = new RecentContentAdapter(this);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(null, 1,
                        LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(gridLayoutManager);
                recyclerView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
            } else {
                emptyTextView.setText("최근 본 영상이 없습니다 :)");
                emptyTextView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFailure(Call<List<VideoLog>> call, Throwable t) {
    }

    @Override
    public void showToast(String title) {
        Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
    }
}
