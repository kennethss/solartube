package com.song.solartube;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by Song on 2017-09-14.
 */

public class ExoRelativeLayout extends FrameLayout {
    public ExoRelativeLayout(Context context) {
        super(context);
    }

    public ExoRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExoRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        //setMeasuredDimension(width, width * 9 / 16);
    }
}
