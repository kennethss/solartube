package com.song.solartube.customlayout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.song.solartube.R;

/**
 * Created by Song on 2018-01-08.
 */

public class OverLayFrameLayout extends View {

    private int brightness;
    private int volume;
    private final int OVERLAY_COLOR;

    private int width;
    private int height;

    private AudioManager audioManager;
    private boolean isScrollBrightness;
    private boolean isScrollVolume;

    private void init(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        if(am != null) {
            this.audioManager = am;
        }

        this.volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    }

    public OverLayFrameLayout(@NonNull Context context) {
        super(context);
        OVERLAY_COLOR = ContextCompat.getColor(context, R.color.colorTealTransparent30);
        init(context);
    }

    public OverLayFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        OVERLAY_COLOR = ContextCompat.getColor(context, R.color.colorTealTransparent30);
        init(context);
    }

    public OverLayFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        OVERLAY_COLOR = ContextCompat.getColor(context, R.color.colorTealTransparent30);
        init(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.width = getMeasuredWidth();
        this.height = getMeasuredHeight();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();
        paint.setColor(OVERLAY_COLOR);

        if(isScrollVolume) {
            float volumeValue = this.height / 15.0f;
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, this.volume, AudioManager.FLAG_SHOW_UI);
            canvas.drawRect(width / 2 , this.height - (volumeValue * this.volume), width,  this.height, paint);

            isScrollVolume = false;

        } else if (isScrollBrightness){
            float brightValue = height / 100.0f;
            float height = this.height - (brightValue * this.brightness);
            canvas.drawRect(0 , height, width / 2,  this.height, paint);

            isScrollVolume = false;
        }
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
        invalidate();
        isScrollBrightness = true;
    }

    public void setVolume(int volume) {
        this.volume = volume;
        this.isScrollVolume = true;
        invalidate();
    }
}
