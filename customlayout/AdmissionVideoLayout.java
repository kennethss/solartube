package com.song.solartube.customlayout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by Song on 2017-12-22.
 */

public class AdmissionVideoLayout extends VideoView{
    public AdmissionVideoLayout(Context context) {
        super(context);
    }

    public AdmissionVideoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdmissionVideoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        setMeasuredDimension(width + (width / 10), height + (height / 10));
    }
}
