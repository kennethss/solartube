package com.song.solartube.player;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.android.exoplayer2.util.Util;
import com.song.solartube.R;
import com.song.solartube.databinding.ModifySettingBinding;
import com.song.solartube.model.Result;
import com.song.solartube.model.User;
import com.song.solartube.singleton.APIManager;
import com.song.solartube.singleton.UserManager;

import java.util.Formatter;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Song on 2017-12-07.
 */

public class CustomModifyRequestPopupView implements View.OnClickListener, Callback<Result> {

    private ModifySettingBinding binding;

    private String mCurrentTime;
    private String id;

    //private View modifyRequestView;
    private PopupWindow popupWindow;
    private StringBuilder formatBuilder = new StringBuilder();
    private Formatter formatter = new Formatter(formatBuilder, Locale.getDefault());

    private void sendModifyRequest(String id, String time, String text) {
        User user = UserManager.getInstance().getUser();
        int userId = 0;
        if (user != null) {
            userId = user.userId;
        }

        APIManager.getInstance().getApiCall().requestModify(id, userId, time, text).enqueue(this);
    }

    public CustomModifyRequestPopupView(LayoutInflater layoutInflater, int height) {
        binding = ModifySettingBinding.inflate(layoutInflater);

        popupWindow = new PopupWindow(binding.getRoot(), height - 200, FrameLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        //popupWindow.update();

        // AddListener
        binding.closeModifyOption.setOnClickListener(this);
        binding.modifySendButton.setOnClickListener(this);
    }

    public boolean isShowing() {
        return popupWindow.isShowing();
    }

    public void showPopupView(String id, long position) {
        this.id = id;
        String time = Util.getStringForTime(formatBuilder, formatter, Math.abs(position));
        mCurrentTime = time;
        binding.modifyTv.setText(time);
        popupWindow.showAtLocation(binding.getRoot(), Gravity.START, 200, 0);
    }

    public void hidePopupView() {
        popupWindow.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_modify_option:
                popupWindow.dismiss();
                break;
            case R.id.modify_send_button:
                sendModifyRequest(id, mCurrentTime, binding.modifyEditText.getText().toString());
                break;
        }
    }

    @Override
    public void onResponse(Call<Result> call, Response<Result> response) {
        if (response.isSuccessful()) {
            Result result = response.body();

            if (result.resultCode == 0) {
                popupWindow.dismiss();
                Toast.makeText(binding.getRoot().getContext(), "전송 성공! 빠른 시일 내 검토 후 수정하겠습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onFailure(Call<Result> call, Throwable t) {

    }
}
