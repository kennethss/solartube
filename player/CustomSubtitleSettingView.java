package com.song.solartube.player;

import android.graphics.Point;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.SeekBar;

import com.song.solartube.R;
import com.song.solartube.databinding.SubtitleSettingBinding;
import com.song.solartube.listener.SubtitleSettingListener;

/**
 * Created by Song on 2017-10-19.
 */

public class CustomSubtitleSettingView implements View.OnClickListener, SeekBar.OnSeekBarChangeListener{


    private SubtitleSettingListener subtitleListener;
    private AppCompatTextView subtitleSizeTextView;
    private AppCompatTextView subtitleHeightTextView;
    private View subtitleSettingView;
    private PopupWindow popupWindow;

    SubtitleSettingBinding binding;

    public CustomSubtitleSettingView(LayoutInflater layoutInflater, int height) {
        final int DEFAULT_SUBTITLE_TEXT_SIZE = 18;
        final int DEFAULT_SUBTITLE_TEXT_HEIGHT = 11;

        binding = SubtitleSettingBinding.inflate(layoutInflater);

        subtitleSettingView = layoutInflater.inflate(R.layout.subtitle_setting, null);
        popupWindow = new PopupWindow(subtitleSettingView, height - 200, FrameLayout.LayoutParams.WRAP_CONTENT);

        ImageView closeButton = subtitleSettingView.findViewById(R.id.close_subtitle_option);
        AppCompatSeekBar textSizeSeekBar = subtitleSettingView.findViewById(R.id.subtitle_size_seek_bar);
        AppCompatSeekBar textHeightSeekBar = subtitleSettingView.findViewById(R.id.subtitle_height_seek_bar);
        subtitleSizeTextView = subtitleSettingView.findViewById(R.id.subtitle_size_text);
        subtitleHeightTextView = subtitleSettingView.findViewById(R.id.subtitle_height_text);

        // Set Defaults
        subtitleSizeTextView.setText(String.valueOf(DEFAULT_SUBTITLE_TEXT_SIZE));
        subtitleHeightTextView.setText(String.valueOf(DEFAULT_SUBTITLE_TEXT_HEIGHT));
        textSizeSeekBar.setProgress(DEFAULT_SUBTITLE_TEXT_SIZE);
        textHeightSeekBar.setProgress(DEFAULT_SUBTITLE_TEXT_HEIGHT);

        // AddListener
        binding.subtitleSizeSeekBar.setOnSeekBarChangeListener(this);
        binding.subtitleHeightSeekBar.setOnSeekBarChangeListener(this);

        textSizeSeekBar.setOnSeekBarChangeListener(this);
        textHeightSeekBar.setOnSeekBarChangeListener(this);
        subtitleSettingView.setOnClickListener(this);
        closeButton.setOnClickListener(this);
    }

    public void showPopupView() {
        popupWindow.showAtLocation(subtitleSettingView, Gravity.START, 200, 0);
    }

    public void hidePopupView() {
        popupWindow.dismiss();
    }

    public boolean isShowing() {
        return popupWindow.isShowing();
    }

    public void setOnSubtitleListener(SubtitleSettingListener subtitleListener) {
        this.subtitleListener = subtitleListener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_subtitle_option:
                popupWindow.dismiss();
                break;
            case R.id.subtitle_size_text:
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        switch (seekBar.getId()){
            case R.id.subtitle_size_seek_bar:
                subtitleListener.onChangedTextSize(i);
                subtitleSizeTextView.setText(String.valueOf(i));
                break;
            case R.id.subtitle_height_seek_bar:
                subtitleListener.onChangedBottomPadding(i);
                subtitleHeightTextView.setText(String.valueOf(i));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
