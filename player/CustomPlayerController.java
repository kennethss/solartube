package com.song.solartube.player;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.util.Util;
import com.song.solartube.R;
import com.song.solartube.customlayout.OverLayFrameLayout;
import com.song.solartube.listener.CustomPlayerControllerListener;
import com.song.solartube.util.DeviceController;

import java.util.Formatter;
import java.util.Locale;

/**
 * Created by Song on 2017-11-17.
 */

public class CustomPlayerController extends BottomSheetBehavior.BottomSheetCallback implements
        DialogInterface.OnShowListener,
        DialogInterface.OnCancelListener,

        View.OnClickListener,
        View.OnTouchListener,
        PlaybackControlView.VisibilityListener,
        GestureDetector.OnGestureListener {

    private GestureDetectorCompat mDetector;
    private OverLayFrameLayout overlay;

    private LinearLayout controllerOptions;
    private FrameLayout controlWholeFrame;
    private FrameLayout controlFrame;

    private FrameLayout playFrame;
    private ImageView moreVertical;
    private ImageView screenLock;
    private ImageView landscapeButton;
    private ImageView arrowBack;
    private TextView controllerTitle;
    private TextView addPosition;

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior bottomSheetBehavior;
    private CustomPlayerControllerListener listener;

    private boolean isLock;
    private boolean isDraggingX = false;
    private boolean isDraggingY;
    private boolean isShowControllerView = true;
    private boolean isLandscape;

    private boolean isAttachedToWindow;

    private long dragMovePosition;

    private final int DISPLAY_WIDTH;
    private final int DISPLAY_HEIGHT;
    private final int MAX_VOLUME = 15;
    private final int MIN_VOLUME = 0;
    private final int SWIPE_THRESHOLD = 100;
    private final float MAX_BRIGHTNESS = 1.0f;
    private final float MIN_BRIGHTNESS = 0.05f;
    private final float DEFAULT_BRIGHTNESS = 0.5f;

    private StringBuilder formatBuilder = new StringBuilder();
    private Formatter formatter = new Formatter(formatBuilder, Locale.getDefault());

    private Window window;
    private AudioManager audioManager;

    private int volume;
    private float volumeFloat;
    private float brightness = 0.2f;

    private final Runnable hideAction = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    private final Runnable hideOverlayFrame = new Runnable() {
        @Override
        public void run() {
            overlay.setVisibility(View.GONE);
        }
    };

    private void hide() {
        controlWholeFrame.setVisibility(View.GONE);
        isShowControllerView = false;
    }

    private void show() {
        controlWholeFrame.removeCallbacks(hideAction);
        controlWholeFrame.setVisibility(View.VISIBLE);
        isShowControllerView = true;
        controlWholeFrame.postDelayed(hideAction, 4000);
    }

    private void hideControlFrame() {
        controlFrame.setVisibility(View.GONE);
    }

    private void showControlFrame() {
        controlFrame.setVisibility(View.VISIBLE);
    }

    private void setDefaultBrightness() {
        WindowManager.LayoutParams params = window.getAttributes();
        this.brightness = params.screenBrightness;

        if(this.brightness < MIN_BRIGHTNESS) {
            this.brightness = DEFAULT_BRIGHTNESS;
        } else {
            this.brightness = params.screenBrightness;
        }
    }

    private void bindingLayouts(Activity activity) {
        landscapeButton= activity.findViewById(R.id.landscape);
        arrowBack = activity.findViewById(R.id.arrow_back);
        controllerOptions = activity.findViewById(R.id.controller_options);
        moreVertical = activity.findViewById(R.id.controller_more);
        screenLock = activity.findViewById(R.id.controller_lock);
        controllerTitle = activity.findViewById(R.id.controller_title);
        addPosition = activity.findViewById(R.id.add_position);
        controlFrame = activity.findViewById(R.id.control_frame);
        controlWholeFrame = activity.findViewById(R.id.control_whole_frame);
        overlay = activity.findViewById(R.id.overlay_frame);
        playFrame = activity.findViewById(R.id.playFrame);
    }

    private void setOnClickListeners() {
        landscapeButton.setOnClickListener(this);
        moreVertical.setOnClickListener(this);
        screenLock.setOnClickListener(this);
        arrowBack.setOnClickListener(this);
    }

    private void initBottomSheet(Activity activity){
        View bottomSheet = activity.getLayoutInflater().inflate(R.layout.control_bottom_sheet, null);
        bottomSheetDialog = new BottomSheetDialog(activity);
        bottomSheetDialog.setContentView(bottomSheet);
        bottomSheetDialog.setOnShowListener(this);
        bottomSheetDialog.setOnCancelListener(this);
        bottomSheetBehavior = BottomSheetBehavior.from((View)bottomSheet.getParent());
        bottomSheetBehavior.setBottomSheetCallback(this);

        LinearLayout fitButton = bottomSheet.findViewById(R.id.control_bottom_fit);
        LinearLayout fillButton = bottomSheet.findViewById(R.id.control_bottom_fill);
        LinearLayout subtitleSetting = bottomSheet.findViewById(R.id.subtitle_setting);
        LinearLayout modifyRequest = bottomSheet.findViewById(R.id.modify_request);

        fitButton.setOnClickListener(this);
        fillButton.setOnClickListener(this);
        subtitleSetting.setOnClickListener(this);
        modifyRequest.setOnClickListener(this);
    }

    private void onClickLockButton() {
        if(isLock) {
            showControlFrame();
            screenLock.setImageResource(R.drawable.ic_lock_open_white_36dp);
        } else {
            hideControlFrame();
            screenLock.setImageResource(R.drawable.ic_lock_outline_white_36dp);
        }

        isLock = !isLock;
    }

    public CustomPlayerController(Activity activity) {
        DeviceController d = new DeviceController();
        Point p = d.getDisplaySize(activity);

        this.window = activity.getWindow();
        this.audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);

        this.DISPLAY_WIDTH = p.x;
        this.DISPLAY_HEIGHT = p.y;

        mDetector = new GestureDetectorCompat(activity, this);

        bindingLayouts(activity);
        setOnClickListeners();
        initBottomSheet(activity);

        show();
    }

    public void setOnCustomPlayerControllerListener(CustomPlayerControllerListener listener) {
        this.listener = listener;
    }

    public void showBottomSheet() {
        final int BOTTOM_SHEET_HEIGHT = 500;
        bottomSheetBehavior.setPeekHeight(BOTTOM_SHEET_HEIGHT);
        bottomSheetDialog.show();
    }

    public void hideBottomSheet() {
        bottomSheetDialog.dismiss();
    }

    public void setStatusIsLandscape(Activity activity, boolean landscape) {
        this.isLandscape = landscape;
        if(landscape) {
            landscapeButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_fullscreen_exit_white_24dp));
        } else {
            landscapeButton.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_fullscreen_white_24dp));
        }
    }

    public void setContentTitle(String title) {
        controllerTitle.setText(title);
    }

    public void setVisibleContentTitle(int visibility) {
        controllerTitle.setVisibility(visibility);
    }

    public void setVisibleMoreVertical(int visibility) {
        controllerOptions.setVisibility(visibility);

        //moreVertical.setVisibility(visibility);
    }

    public FrameLayout getPlayFrame() {
        return playFrame;
    }

    // Gesture
    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        if(isShowControllerView) {
            hide();
        } else {
            if(isLock) {
                show();
                hideControlFrame();
            } else {
                show();
                showControlFrame();
            }
        }

        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        if(!isLock){
            float deltaY = motionEvent1.getY() - motionEvent.getY();
            float deltaX = motionEvent1.getX() - motionEvent.getX();

            if (Math.abs(deltaX) > Math.abs(deltaY)) { // 좌우 스크롤
                if (Math.abs(deltaX) > SWIPE_THRESHOLD) {
                    if (!isDraggingX && !isDraggingY) {
                        listener.onDragStart(false);
                        isDraggingX = true;
                        hide();
                    } else if(isDraggingX) {
                        addPosition.setVisibility(View.VISIBLE);
                        dragMovePosition = (long)deltaX * 30;
                        String time = Util.getStringForTime(formatBuilder, formatter, Math.abs(dragMovePosition));

                        if (dragMovePosition > 1000) { // 그냥 클릭시 이동이되는걸 막기위함
                            String text = "+" + time;
                            addPosition.setText(text);
                        } else if (dragMovePosition < -1000){
                            String text = "-" + time;
                            addPosition.setText(text);
                        }
                    }

                /*if (deltaX > 0) {
                    //Log.i(TAG, "Slide right");
                } else {
                    //Log.i(TAG, "Slide left");
                }*/
                }
            } else { // 상하 스크롤
                if(!isDraggingX && !isDraggingY) {
                    listener.onDragStart(true);
                    isDraggingY = true;
                    hide();
                    this.overlay.setVisibility(View.VISIBLE);
                }

                if(isDraggingY) {
                    float value = (this.DISPLAY_WIDTH - motionEvent1.getY()) / 1000; // 디스플레이대비 값
                    float defaultPoint = (this.DISPLAY_WIDTH - motionEvent.getY()) / 1000; // 찍었을때의 값
                    float b = defaultPoint - value;     // 총 이동거리

                    final int WIDTH;
                    final float DEFAULT_POINT = motionEvent.getX();

                    if(this.isLandscape) {
                        WIDTH = DISPLAY_HEIGHT;
                    } else {
                        WIDTH = DISPLAY_WIDTH;
                    }

                    if(WIDTH / 2 < DEFAULT_POINT) {
                        addPosition.setVisibility(View.VISIBLE);
                        int volume = -(int)(b * 10);

                        volumeFloat = this.volume + volume;

                        int adjustVolume = 0;

                        adjustVolume = (int)volumeFloat;

                        if(adjustVolume > MAX_VOLUME) {
                            adjustVolume = MAX_VOLUME;
                        } else if(adjustVolume < MIN_VOLUME){
                            adjustVolume = MIN_VOLUME;
                        }

                        //audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, adjustVolume, AudioManager.FLAG_PLAY_SOUND);

                        addPosition.setText(toString().valueOf(adjustVolume));
                        overlay.setVolume(adjustVolume);

                    } else {
                        float a = -(Float.parseFloat(String.format(Locale.KOREAN,"%.2f", b)));

                        //float value = (this.DISPLAY_WIDTH / Math.abs(deltaY))/ 10; // 드래그 양의 값
                        addPosition.setVisibility(View.VISIBLE);

                        float realBrightness;

                        if(this.brightness + a >= MAX_BRIGHTNESS) {
                            realBrightness = MAX_BRIGHTNESS;
                        } else if(this.brightness + a <= MIN_BRIGHTNESS){
                            realBrightness = MIN_BRIGHTNESS;
                        } else {
                            realBrightness = this.brightness + a;
                        }

                        WindowManager.LayoutParams params = window.getAttributes();
                        params.screenBrightness = realBrightness;
                        window.setAttributes(params);

                        addPosition.setText(String.valueOf((int)(realBrightness * 100)));
                        overlay.setBrightness((int)(realBrightness * 100));
                    }
                }

            /*if (Math.abs(deltaY) > SWIPE_THRESHOLD) { // 최소 드래그 길이

            }*/

                // 리스너쪽 전달
            /*if (deltaY > 0) {
                //Log.d("델타 Y", "디스플레이 = " + a +" 델타 Y = " + deltaY);
                //listener.onDragBrightness(a);
            } else {
                //listener.onDragBrightness(a);
                //Log.d("델타 Y", "디스플레이 = " + a +" 델타 Y = " + deltaY);
            }*/
            }
        }

        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        mDetector.onTouchEvent(motionEvent);

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setDefaultBrightness();
                this.volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                controlWholeFrame.removeCallbacks(hideAction);
                isAttachedToWindow = false;
                break;

            case MotionEvent.ACTION_MOVE:
                break;

            case MotionEvent.ACTION_UP:
                if(isDraggingX) {
                    listener.onDragEnd(dragMovePosition);
                    isDraggingX = false;
                    dragMovePosition = 0;
                    addPosition.setVisibility(View.GONE);
                } else if(isDraggingY) {
                    isDraggingY = false;
                    this.overlay.removeCallbacks(hideOverlayFrame);
                    this.overlay.postDelayed(hideOverlayFrame, 250);
                    addPosition.setVisibility(View.GONE);
                }

                isAttachedToWindow = true;
                break;
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        show();

        switch (view.getId()) {

            case R.id.landscape:
                listener.onClickLandScape();
                break;

            case R.id.arrow_back:
                listener.onClickArrowBack();
                break;
            case R.id.controller_lock:
                onClickLockButton();
                break;
            case R.id.controller_more:
                listener.onClickControllerMore();
                break;

            case R.id.control_bottom_fit:
                listener.onClickResizeFit();
                break;

            case R.id.control_bottom_fill:
                listener.onClickResizeFill();
                break;

            case R.id.subtitle_setting:
                listener.onClickSubtitleOption();
                break;
            case R.id.modify_request:
                listener.onClickModifyRequest();
                break;
        }
    }

    @Override
    public void onVisibilityChange(int visibility) {
    }

    @Override
    public void onShow(DialogInterface dialogInterface) {
        listener.onShowBottomSheet();
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        listener.onHideUiOptions();
    }

    @Override
    public void onStateChanged(@NonNull View bottomSheet, int newState) {
        if(newState == BottomSheetBehavior.STATE_DRAGGING) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }

        listener.onHideUiOptions();
    }

    @Override
    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        listener.onHideUiOptions();
    }
}
